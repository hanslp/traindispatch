package edu.ntnu.stud.controller;

import edu.ntnu.stud.exceptions.BackInTimeException;
import edu.ntnu.stud.exceptions.NextDayException;
import edu.ntnu.stud.models.Time;
import edu.ntnu.stud.models.Train;
import edu.ntnu.stud.models.TrainRegister;
import java.time.LocalTime;
import java.util.List;
import java.util.Scanner;

/**
 * Provides services for the options in {@link edu.ntnu.stud.view.Menu}:. <br>
 * <ul>
 *     <li> Print overview of trains:
 *     {@link TrainController#getOverview()} </li>
 *     <li> Adding new train:
 *     {@link TrainController#addTrain(Scanner)} </li>
 *     <li> Allocating track to train:
 *     {@link TrainController#setTrackByTrainId(Scanner)} </li>
 *     <li> Allocating delay to train:
 *     {@link TrainController#setDelayByTrainId(Scanner)} </li>
 *     <li> Searching for expected departure using train ID:
 *     {@link TrainController#getDepartureByTrainId(Scanner)} </li>
 *     <li> Searching for expected departure using destination:
 *     {@link TrainController#getDepartureByDestination(Scanner)} </li>
 *     <li> Deleting train from list:
 *     {@link TrainController#deleteTrainByTrainId(Scanner)} </li>
 *     <li> Updating time reference:
 *     {@link TrainController#updateTime(Scanner)} </li>
 * </ul>
 */
public class TrainController {
  private final Time time;
  private final TrainRegister trainRegister;
  private final InputValidator inputValidator;

  /**
   * Constructor uses dependency injection to get references to the Time and
   * TrainRegister.
   *
   * @param time          Represents the current time,
   *                      used in filtering already departed trains.
   * @param trainRegister Manages list of trains, used for displaying,
   *                      adding, deleting and searching trains.
   */
  public TrainController(Time time, TrainRegister trainRegister) {
    this.time = time;
    this.trainRegister = trainRegister;
    inputValidator = new InputValidator(this.trainRegister);
  }

  /**
   * Checks if train has expected departure time after or equal to current time.
   *
   * @param expectedDeparture - Expected departure time of train.
   * @return True if train has not departed, false if train has departed.
   * @see Train#getExpectedDeparture() calculating expected departure time.
   */
  private boolean notDeparted(String expectedDeparture) {
    LocalTime departureTime = LocalTime.parse(expectedDeparture);
    return departureTime.equals(time.getTime()) || departureTime.isAfter(time.getTime());
  }

  /**
   * Prints all trains that have expected departure time after or equal to current time.  <br>
   * Trains are sorted by expected departure time. <br>
   * Overview contains: <br>
   * <ul>
   *     <li> Train ID </li>
   *     <li> Destination </li>
   *     <li> Departure time </li>
   *     <li> Expected departure time </li>
   *     <ul>
   *         <li> If train has no delay (00:00), expected departure is shown as "On time". </li>
   *     </ul>
   *     <li> Line </li>
   *     <li> Track </li>
   *     <ul>
   *         <li> If track has track number (-1), track is shown as "TBD" (To Be Determined) </li>
   *     </ul>
   * </ul>
   *
   * @see Train#getExpectedDeparture() calculating expected departure time
   * @see TrainRegister#getSortedTrainList() sorting trains by expected departure time
   * @see TrainController#notDeparted(String) filtering trains that have already departed.
   */
  public void getOverview() {
    System.out.println("\n————————————————————————————(Time: " + time.getTime()
        + ")—————————————————————————————");
    System.out.printf("| %-8s | %-16s | %-9s | %-8s | %-5s | %-5s |%n",
        "Train ID", "Destination", "Departure", "Expected", "Line", "Track");
    System.out.println("——————————————————————————————————————————————————————————————————————");

    trainRegister.getSortedTrainList().stream()
        .filter(train -> notDeparted(train.getExpectedDeparture()))
        .forEach(train -> {
          // Check if the train has no delay; if so, show expectedDeparture as "On time"
          String expectedDeparture = (train.getDelay().equals("00:00"))
              ? "On time" : train.getExpectedDeparture();

          // Check if the track is (-1); if so, show track as "TBD"
          String track = (train.getTrack() == -1)
              ? "TBD" : String.valueOf(train.getTrack());

          System.out.printf("| %-8d | %-16s | %-9s | %-8s | %-5s | %-5s |%n",
              train.getTrainId(), train.getDestination(), train.getDepartureTime(),
              expectedDeparture, train.getLine(), track);
        });

    System.out.println("——————————————————————————————————————————————————————————————————————");
  }

  /**
   * Takes input needed to create a new train instance: <br>
   * <ul>
   *     <li> Departure time </li>
   *     <li> Line </li>
   *     <li> Train ID </li>
   *     <li> Destination </li>
   *     <li> Track </li>
   * </ul>
   * Delay is set to "00:00" by default. <br>
   * <p>
   *     During each prompt for input, user has the option to return to the main menu.<br>
   *     (-2) is used to indicate return to main menu
   * </p>
   *
   * @see InputValidator#validateString(Scanner, String, boolean)
   *     validating departure time, line and destination input
   * @see InputValidator#validateTrain(Scanner, String, boolean) validating train ID input
   * @see InputValidator#validateTrack(Scanner, String) validating track input
   * @see TrainRegister#isTrainIdUnique(int)
   * @see TrainRegister#addTrainToList(String, String, int, String, int, String)
   *     adding train to registry
   */
  public void addTrain(Scanner scanner) {
    System.out.println("\nAdd new train (Enter 'return' to go back to the main menu)");
    boolean backToMenu = false;

    while (!backToMenu) {
      // Departure time
      String departureTime = inputValidator.validateString(scanner,
          "Enter departure time (hh:mm): ", true);
      if ("-2".equals(departureTime)) {
        backToMenu = true;
        continue;
      }

      // Line
      String line = inputValidator.validateString(scanner, "Enter line: ", false);
      if ("-2".equals(line)) {
        backToMenu = true;
        continue;
      }

      // Train ID
      int trainId = inputValidator.validateTrain(scanner, "Enter train ID: ", true);
      if (trainId == -2) {
        backToMenu = true;
        continue;
      }

      // Destination
      String destination = inputValidator.validateString(scanner, "Enter destination: ", false);
      if ("-2".equals(destination)) {
        backToMenu = true;
        continue;
      }

      // Track
      int track = inputValidator.validateTrack(scanner, "Enter track (leave blank to assign -1): ");
      if (track == -2) {
        backToMenu = true;
        continue;
      }

      // Default delay
      String delay = "00:00";

      trainRegister.addTrainToList(departureTime, line, trainId, destination, track, delay);
      System.out.println("\nNew train added with train ID: " + trainId);
      backToMenu = true;
    }
  }

  /**
   * Takes input needed to allocate a track to a train: <br>
   * <ul>
   *     <li> Train ID </li>
   *     <li> New track </li>
   * </ul>
   * Track is set to (-1) if user input for new track is empty. <br>
   * Track number (-1) indicates that the is track not yet determined. <br>
   * <p>
   *     During each prompt for input, user has the option to return to the main menu.<br>
   *     (-2) is used to indicate return to main menu.
   * </p>
   *
   * @see InputValidator#validateTrain(Scanner, String, boolean) validating train ID input
   * @see InputValidator#validateTrack(Scanner, String) validating new track input
   */
  public void setTrackByTrainId(Scanner scanner) {
    System.out.println("\nAllocate track (Enter 'return' to go back to the main menu)");
    boolean backToMenu = false;

    while (!backToMenu) {
      int trainId = inputValidator.validateTrain(scanner,
          "Enter train ID to allocate track: ", false);
      if (trainId == -2) { // -2 indicates return
        backToMenu = true;
      } else {
        Train train = trainRegister.searchTrainId(trainId);

        if (train != null) {
          int newTrack = inputValidator.validateTrack(scanner,
              "Enter new track (leave blank to assign -1): ");

          // -2 indicates return
          if (newTrack != -2) {
            train.setTrack(newTrack);
            System.out.println("\nTrain ID: " + trainId + ", updated track to: " + newTrack);
          }
          backToMenu = true;
        } else {
          System.out.println("Invalid input: Train ID '" + trainId + "' not found.");
        }
      }
    }
  }

  /**
   * Takes input needed to allocate a delay to a train: <br>
   * <ul>
   *     <li> Train ID </li>
   *     <li> New delay </li>
   * </ul>
   * New delay may result in {@link NextDayException} being thrown. <br>
   * <p>
   *     During each prompt for input, user has the option to return to the main menu.<br>
   *     (-2) is used to indicate return to main menu.
   * </p>
   *
   * @see InputValidator#validateTrain(Scanner, String, boolean) validating train ID input
   * @see InputValidator#validateString(Scanner, String, boolean) validating new delay input
   * @see TrainRegister#searchTrainId(int) searching for train with matching train ID
   * @see Train#getExpectedDeparture() calculating expected departure time
   * @see Train#setDelay(String) throwing NextDayException if expected departure exceeds 24 hours
   */
  public void setDelayByTrainId(Scanner scanner) {
    System.out.println("\nAllocate delay (Enter 'return' to go back to the main menu)");
    boolean backToMenu = false;

    while (!backToMenu) {
      int trainId = inputValidator.validateTrain(
          scanner, "Enter train ID to allocate delay: ", false);
      if (trainId == -2) { // -2 indicates return
        backToMenu = true;

      } else {
        Train train = trainRegister.searchTrainId(trainId);
        if (train != null) {
          System.out.println("Train ID: " + trainId + ", current delay: " + train.getDelay()
              + ", current departure time: " + train.getDepartureTime());

          boolean validTrack = false;
          while (!validTrack && !backToMenu) {
            String newDelay = inputValidator.validateString(
                scanner, "Enter new delay (hh:mm): ", true);
            if ("-2".equals(newDelay)) { // -2 indicates return
              backToMenu = true;

            } else {
              try {
                train.setDelay(newDelay);
                System.out.println("\nTrain ID: " + trainId + ", new delay: " + newDelay
                    + ", new expected departure: " + train.getExpectedDeparture());
                validTrack = true;
                backToMenu = true;
              } catch (NextDayException e) {
                System.out.println(e.getMessage());
              }
            }
          }
        } else {
          System.out.println("Invalid input: Train ID: " + trainId + " not found.");
        }
      }
    }
  }

  /**
   * Takes input (TrainID) and retrieves expected departure time for matching train. <br>
   * <p>
   * Train with expected departure time before current time is shown as already departed <br>
   * </p>
   * <p>
   * During prompt for input, user has option to return to the main menu.<br>
   * (-2) is used to indicate return to main menu.
   * </p>
   *
   * @see InputValidator#validateTrain(Scanner, String, boolean) validating train ID input
   * @see TrainRegister#searchTrainId(int) searching for train with matching train ID
   * @see Train#getExpectedDeparture() calculating expected departure time
   * @see TrainController#notDeparted(String) filtering trains that have already departed
   */
  public void getDepartureByTrainId(Scanner scanner) {
    System.out.println("\nSearch for expected departure "
        + "(Enter 'return' to go back to the main menu)");
    boolean backToMenu = false;

    while (!backToMenu) {
      int trainId = inputValidator.validateTrain(scanner,
          "Enter a train ID to find expected departure: ", false);
      if (trainId == -2) { // -2 indicates return
        backToMenu = true;
      } else {
        Train matchingTrain = trainRegister.searchTrainId(trainId);

        if (matchingTrain != null) {
          if (notDeparted(matchingTrain.getExpectedDeparture())) {
            System.out.println("\nTrain ID: " + matchingTrain.getTrainId()
                + ", expected departure: " + matchingTrain.getExpectedDeparture());
          } else {
            System.out.println("\nTrain ID: " + matchingTrain.getTrainId()
                + ", already departed at: " + matchingTrain.getExpectedDeparture());
          }
          backToMenu = true;
        } else {
          System.out.println("Train ID: " + trainId + " not found.");
        }
      }
    }
  }

  /**
   * Takes input (destination) and retrieves expected departure time for matching trains. <br>
   * <p>
   * Trains that have already departed are filtered out. <br>
   * Trains are sorted by expected departure time. <br>
   * </p>
   * <p>
   * During prompt for input, user has option to return to the main menu.<br>
   * (-2) is used to indicate return to main menu.
   * </p>
   *
   * @see InputValidator#validateString(Scanner, String, boolean) validating destination input
   * @see TrainRegister#searchDestination(String) searching for trains with matching destination
   * @see Train#getExpectedDeparture() calculating expected departure time
   * @see TrainController#notDeparted(String) filtering trains that have already departed
   */
  public void getDepartureByDestination(Scanner scanner) {
    System.out.println("\nSearch for train departure (Enter 'return' to go back to the main menu)");
    boolean backToMenu = false;

    while (!backToMenu) {
      String destination = inputValidator.validateString(scanner,
          "Enter a destination to find expected departure: ", false);
      if ("-2".equals(destination)) {
        backToMenu = true;
      } else {
        List<Train> matchingTrains = trainRegister.searchDestination(destination);

        // Filter and check for matching trains
        List<Train> notDepartedList = matchingTrains.stream()
            .filter(train -> notDeparted(train.getExpectedDeparture()))
            .toList();

        if (!notDepartedList.isEmpty()) {
          System.out.println("\nTrains with destination '" + destination + "':");

          notDepartedList.forEach(train -> System.out.println("Train ID: " + train.getTrainId()
              + ", expected departure: " + train.getExpectedDeparture()));
          backToMenu = true;
        } else {
          System.out.println("No trains with destination '" + destination + "' found.");
        }
      }
    }
  }

  /**
   * Takes input (TrainID) and deletes matching train from train list. <br>
   * <p>
   * During prompt for input, user has option to return to the main menu.<br>
   * (-2) is used to indicate return to main menu.
   * </p>
   *
   * @see InputValidator#validateTrain(Scanner, String, boolean) validating train ID input
   * @see TrainRegister#deleteTrainFromList(int) deleting train from registry
   */
  public void deleteTrainByTrainId(Scanner scanner) {
    System.out.println("\nDelete train from train list (enter 'return' to exit): ");
    boolean backToMenu = false;

    while (!backToMenu) {
      int trainId = inputValidator.validateTrain(scanner, "Enter Train ID to delete: ", false);
      if (trainId == -2) { // -2 indicates return
        backToMenu = true;
      } else {
        boolean deleted = trainRegister.deleteTrainFromList(trainId);
        if (deleted) {
          System.out.println("\nTrain ID: " + trainId + " has been deleted.");
          backToMenu = true;
        } else {
          System.out.println("Invalid input: Train ID: " + trainId + " not found.");
        }
      }
    }
  }

  /**
   * Takes input in (hh:mm) format and updates time reference. <br>
   * <p>
   * During prompt for input, user has option to return to the main menu.<br>
   * (-2) is used to indicate return to main menu.
   * </p>
   *
   * @see InputValidator#validateString(Scanner, String, boolean) validating time input
   * @see Time#setSystemTime(String) ensuring that new time is not before current time
   */
  public void updateTime(Scanner scanner) {
    System.out.println("\nUpdate time (Enter 'return' to go back to the main menu)");
    System.out.println("Current time: " + time.getTime());
    boolean backToMenu = false;
    boolean chronological = false;

    while (!backToMenu && !chronological) {
      String newTime = inputValidator.validateString(scanner, "Enter updated time (hh:mm): ", true);
      if ("-2".equals(newTime)) {
        backToMenu = true;
      } else {
        try {
          time.setSystemTime(newTime);
          chronological = true;
          backToMenu = true;
          System.out.println("\nTime updated to: " + time.getTime());
        } catch (BackInTimeException c) {
          System.out.println(c.getMessage());
        }
      }
    }
  }
}