package edu.ntnu.stud.controller;

import edu.ntnu.stud.exceptions.TimeFormatException;
import edu.ntnu.stud.models.Time;
import edu.ntnu.stud.models.TrainRegister;
import java.util.Scanner;

/**
 * Responsible for validating user input for {@link TrainController}. Provides methods for:
 * <ul>
 *     <li>Validating string input:
 *     {@link InputValidator#validateString(Scanner, String, boolean)}</li>
 *     <li>Validating integer input:
 *     {@link InputValidator#validateTrain(Scanner, String, boolean)}</li>
 *     <li>Validating track input:
 *     {@link InputValidator#validateTrack(Scanner, String)}</li>
 * </ul>
 */
public class InputValidator {
  private final TrainRegister trainRegister;

  /**
   * Constructs an InputValidator with a reference to the {@link TrainRegister} for
   * validation of train ID uniqueness.
   *
   * @param trainRegister used for train ID uniqueness validation.
   */
  public InputValidator(TrainRegister trainRegister) {
    this.trainRegister = trainRegister;
  }

  /**
   * Loops prompt and validation for string input. The validations concern:
   * <ul>
   *     <li> If user input is "return", <STRONG>{@code input}</STRONG> is set to "-2",
   *     indicating that user wants to exit loop. </li>
   *     <li> If input is empty, user is informed that input is invalid,
   *     and prompted for new input. </li>
   *     <li> If <STRONG>{@code validateTime}</STRONG> is true,
   *     the method calls on {@link Time#validateTime(String)}
   *     to validate if input has (hh:mm) format. </li>
   * </ul>
   *
   * @param scanner      Scanner object for reading user input.
   * @param prompt       Prompt message displaying expected input.
   * @param validateTime Indicates if input should be validated for (hh:mm) format.
   * @return Valid string input or "-2"
   */
  public String validateString(Scanner scanner, String prompt, boolean validateTime) {
    boolean backToMenu = false;
    boolean emptyInput = true;
    boolean validTime = false;
    String input = null;

    while (!backToMenu && (emptyInput || (validateTime && !validTime))) {
      System.out.print(prompt);
      input = scanner.nextLine();

      // Check for empty input
      if (input.trim().isEmpty()) {
        System.out.println("Invalid input: empty input.");
        emptyInput = true;
      } else {
        emptyInput = false;
      }

      // Check for 'return'
      if (!emptyInput && input.equalsIgnoreCase("return")) {
        System.out.println("Returning to the main menu.");
        backToMenu = true;
        input = "-2"; // indicates return
      }

      // Validate time if required
      if (validateTime && (!emptyInput && !backToMenu)) {
        try {
          Time.validateTime(input);
          validTime = true;
        } catch (TimeFormatException e) {
          System.out.println(e.getMessage());
        }
      }
    }
    return input;
  }

  /**
   * Loops prompt and validation for input associated with the
   * <STRONG>{@code trainID}</STRONG> parameter. <br>
   * The validations concern:
   * <ul>
   *     <li> If user input is "return", <STRONG>{@code intInput}</STRONG> is set to (-2),
   *     indicating that user wants to exit loop. </li>
   *     <li> If input is empty, user is informed that input is invalid,
   *     and prompted for new input. </li>
   *     <li> If input is not a numeric value, user is informed that input is invalid,
   *     and prompted for new input. </li>
   *     <li> If <STRONG>{@code validateTrainId}</STRONG> is true, the method calls on
   *     {@link TrainRegister#isTrainIdUnique(int)} to validate if
   *     the provided <STRONG>{@code TrainID}</STRONG>
   *     is unique. </li>
   * </ul>
   *
   * @param scanner         Scanner object for reading user input.
   * @param prompt          Prompt message displaying expected input.
   * @param validateTrainId Indicates if input should be validated for uniqueness.
   * @return Valid train ID number or (-2).
   */
  public int validateTrain(Scanner scanner, String prompt, boolean validateTrainId) {
    boolean backToMenu = false;
    boolean isInteger = false;
    int intInput = 0;

    while (!backToMenu && !isInteger
        || (validateTrainId && !trainRegister.isTrainIdUnique(intInput))) {
      System.out.print(prompt);
      String input = scanner.nextLine();

      // Check for 'return'
      if (!input.trim().isEmpty() && input.equalsIgnoreCase("return")) {
        System.out.println("Returning to the main menu.");
        backToMenu = true;
        intInput = -2; // indicates return
      } else {

        // Check for integer input
        try {
          intInput = Integer.parseInt(input);
          isInteger = true;
        } catch (NumberFormatException e) {
          System.out.println("Invalid input: not an integer.");
        }

        // Check train ID for uniqueness if required
        if (validateTrainId) {
          if (!trainRegister.isTrainIdUnique(intInput)) {
            System.out.println("Invalid input: Train ID already in use.");
            isInteger = false;  // Reset isNumeric if train ID is not unique
          }
        }
      }
    }
    return intInput;
  }

  /**
   * Loops prompt and validation for input associated with the
   * <STRONG>{@code track}</STRONG> parameter. <br>
   * The validations concern:
   * <ul>
   *     <li> If user input is "return", <STRONG>{@code intInput}</STRONG> is set to (-2),
   *     indicating that user wants to exit loop. </li>
   *     <li> If input is empty, <STRONG>{@code intInput}</STRONG> is set to (-1), indicating that
   *     the track is not yet determined. </li>
   *     <li> If input is not a numeric value, user is informed that input is invalid,
   *     and prompted for new input. </li>
   * </ul>
   *
   * @param scanner Scanner object for reading user input.
   * @param prompt  Prompt message displaying expected input.
   * @return Valid track number, <STRONG>or</STRONG> (-1), <STRONG>or</STRONG> (-2).
   */
  public int validateTrack(Scanner scanner, String prompt) {
    boolean backToMenu = false;
    boolean isInteger = false;
    int intInput = 0;

    while (!backToMenu && !isInteger) {
      System.out.print(prompt);
      String input = scanner.nextLine();

      // Check for empty input, indicates track -1
      if (input.trim().isEmpty()) {
        input = "-1";
      }

      // Check for 'return'
      if (input.equalsIgnoreCase("return")) {
        System.out.println("Returning to the main menu.");
        backToMenu = true;
        input = "-2"; // indicates return
      }

      // Validate numeric input
      try {
        intInput = Integer.parseInt(input);
        isInteger = true;
      } catch (NumberFormatException e) {
        System.out.println("Invalid input: not an integer.");
      }
    }
    return intInput;
  }
}