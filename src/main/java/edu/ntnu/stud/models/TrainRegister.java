package edu.ntnu.stud.models;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Responsible for managing a registry of {@link Train} objects. Provides methods for:
 * <ul>
 *     <li> Adding new train to registry:
 *     {@link TrainRegister#addTrainToList(String, String, int, String, int, String)} </li>
 *     <li> Providing list of trains sorted by expected departure time:
 *     {@link TrainRegister#getSortedTrainList()} </li>
 *     <li> Searching for train by train ID:
 *     {@link TrainRegister#searchTrainId(int)} </li>
 *     <li> Searching for train by destination:
 *     {@link TrainRegister#searchDestination(String)} </li>
 *     <li> Deleting train from registry:
 *     {@link TrainRegister#deleteTrainFromList(int)} </li>
 *     <li> Checking if train ID is unique:
 *     {@link TrainRegister#isTrainIdUnique(int)} </li>
 * </ul>
 */
public class TrainRegister {
  private final List<Train> trainList = new ArrayList<>();

  /**
   * Creates new Train object and adds it to train registry.
   *
   * @param departureTime Departure time of train in format (hh:mm).
   * @param line          Specifies route traversed by train.
   * @param trainId       Unique number identifying train.
   * @param destination   Destination of the train.
   * @param track         Specifies which track the train departs from.
   * @param delay         Delay time of train in format "hh:mm".
   */
  public void addTrainToList(
      String departureTime, String line, int trainId, String destination, int track, String delay) {
    Train newTrain = new Train(departureTime, line, trainId, destination, track, delay);
    trainList.add(newTrain);
  }

  /**
   * Provides list of Train objects, and sorts them by expected departure times.
   *
   * @return Sorted list of trains.
   * @see Train#getExpectedDeparture() calculation of expected departure time.
   */
  public List<Train> getSortedTrainList() {
    List<Train> departureSortedTrainList = new ArrayList<>(trainList);
    departureSortedTrainList.sort(Comparator.comparing(Train::getExpectedDeparture));
    return departureSortedTrainList;
  }

  /**
   * Finds Train object in registry based on train ID.
   *
   * @param trainId Integer to search for.
   * @return Train object if found, null if Train object was not found.
   */
  public Train searchTrainId(int trainId) {
    return trainList.stream()
        .filter(train -> train.getTrainId() == trainId)
        .findFirst()
        .orElse(null);
  }

  /**
   * Finds Train objects in registry based on destination,
   * and sorts them by expected departure time.
   *
   * @param destination String to search for.
   * @return List of trains with provided destination, sorted by expected departure time.
   * @see Train#getExpectedDeparture() calculation of expected departure time.
   */
  public List<Train> searchDestination(String destination) {
    return trainList.stream()
        .filter(train -> train.getDestination().equalsIgnoreCase(destination))
        .sorted(Comparator.comparing(Train::getExpectedDeparture))
        .toList();
  }

  /**
   * Deletes Train object from registry based on train ID.
   *
   * @param trainId Integer to search for.
   * @return True if Train object was deleted, false if Train object was not deleted.
   */
  public boolean deleteTrainFromList(int trainId) {
    return trainList.removeIf(train -> train.getTrainId() == trainId);
  }

  /**
   * Checks if train ID is unique within registry.
   *
   * @param trainId Train ID to check for uniqueness.
   * @return True if train ID is unique, false if train ID is not unique.
   */
  public boolean isTrainIdUnique(int trainId) {
    return trainList.stream()
        .noneMatch(train -> train.getTrainId() == trainId);
  }
}