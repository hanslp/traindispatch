package edu.ntnu.stud.models;

import edu.ntnu.stud.exceptions.NextDayException;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Responsible for managing dispatch information regarding a train. Provides methods for:
 * <ul>
 *     <li>Calculating the expected departure time of the train:
 *     {@link Train#getExpectedDeparture()}</li>
 *     <li>Getting and setting information regarding the dispatch of the train</li>
 * </ul>
 */
public final class Train {
  private final String departureTime;
  private final String line;
  private final int trainId;
  private final String destination;
  private int track;
  private String delay;

  private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");

  /**
   * Constructs a Train object with the provided details.
   *
   * @param departureTime The departure time of the train, in (hh:mm) format.
   * @param line          The train line identifier.
   * @param trainId       The unique train identifier.
   * @param destination   The destination of the train.
   * @param delay         The delay time of the train, in (hh:mm) format.
   * @param track         The track number of the train.
   */
  public Train(
      String departureTime, String line, int trainId, String destination, int track, String delay) {
    this.departureTime = departureTime;
    this.line = line;
    this.trainId = trainId;
    this.destination = destination;
    this.track = track;
    this.delay = delay;
  }

  /**
   * Calculates the sum of the departure time and delay time of the train.<br>
   *
   * @return Expected departure time in (hh:mm) format.
   */
  public String getExpectedDeparture() {
    LocalTime currentDeparture = LocalTime.parse(departureTime);
    LocalTime delayTime = LocalTime.parse(delay);
    LocalTime expectedDeparture = currentDeparture
        .plusHours(delayTime.getHour()).plusMinutes(delayTime.getMinute());

    return expectedDeparture.format(TIME_FORMATTER);
  }

  /**
   * Provides departure time of train.
   *
   * @return Departure time of train in (hh:mm) format.
   */
  public String getDepartureTime() {
    return departureTime;
  }

  /**
   * Provides train line identifier.
   *
   * @return Train line identifier.
   */
  public String getLine() {
    return line;
  }

  /**
   * Provides train ID.
   *
   * @return Train ID.
   */
  public int getTrainId() {
    return trainId;
  }

  /**
   * Provides train destination.
   *
   * @return Train destination.
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Provides delay time of train.
   *
   * @return Delay time of train in (hh:mm) format.
   */
  public String getDelay() {
    return delay;
  }

  /**
   * Provides track number of train.
   *
   * @return Track number of train.
   */
  public int getTrack() {
    return track;
  }

  /**
   * Updates track number of train.
   *
   * @param newTrack New track number.
   */
  public void setTrack(int newTrack) {
    this.track = newTrack;
  }

  /**
   * Updates delay time of train and checks
   * if resultant duration is less than the duration of a day.<br>
   *
   * @param newDelay New delay time in (hh:mm) format.
   * @throws NextDayException If the expected departure exceeds 24 hours
   */
  public void setDelay(String newDelay) throws NextDayException {
    LocalTime departureTime = LocalTime.parse(this.departureTime);
    LocalTime delay = LocalTime.parse(newDelay);

    // Calculate delay duration and departure duration
    Duration delayDuration = Duration.ofHours(delay.getHour()).plusMinutes(delay.getMinute());
    Duration departureDuration = Duration.ofHours(departureTime.getHour())
        .plusMinutes(departureTime.getMinute());

    // Calculate the total duration
    Duration totalDuration = delayDuration.plus(departureDuration);

    // Check if the total duration exceeds the duration of a day
    if (totalDuration.compareTo(Duration.ofDays(1)) > 0) {
      throw new NextDayException();
    } else {
      this.delay = newDelay;
    }
  }
}