package edu.ntnu.stud.models;

import edu.ntnu.stud.exceptions.BackInTimeException;
import edu.ntnu.stud.exceptions.TimeFormatException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * Responsible for controlling time in the system. Provides methods for:
 * <ul>
 *     <li>Validating time format:
 *     {@link Time#validateTime(String)}</li>
 *     <li>Validating that the updated system time is after the current time:
 *     {@link Time#setSystemTime(String)}</li>
 *     <li>Getting and setting time.</li>
 * </ul>
 */
public class Time {
  private LocalTime time;
  private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");

  /**
   * Constructs Time object with default time (00:00). <br>
   */
  public Time() {
    this.time = LocalTime.parse("00:00", TIME_FORMATTER);
  }

  /**
   * Provides time in (hh:mm) format.
   *
   * @return time in (hh:mm) format.
   */
  public LocalTime getTime() {
    return time;
  }

  /**
   * Validates format (hh:mm) of a time string.
   *
   * @param timeToValidate time string to validate.
   * @throws TimeFormatException If the time string has an invalid format.
   */
  public static void validateTime(String timeToValidate) throws TimeFormatException {
    try {
      // Attempt to parse the time string
      LocalTime parsedTime = LocalTime.parse(timeToValidate);
    } catch (DateTimeParseException e) {
      throw new TimeFormatException();
    }
  }

  /**
   * Ensures that the new time is after the current time.
   *
   * @param newTime new time reference.
   * @throws BackInTimeException If the new time is earlier or equal to the current time.
   */
  public void setSystemTime(String newTime) throws BackInTimeException {
    LocalTime newTimeParsed = LocalTime.parse(newTime, TIME_FORMATTER);

    // Ensure that the new time is not earlier than the current time
    if (newTimeParsed.isBefore(getTime()) || newTimeParsed.equals(getTime())) {
      throw new BackInTimeException();
    }
    this.time = newTimeParsed;
  }
}