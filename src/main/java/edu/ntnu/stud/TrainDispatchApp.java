package edu.ntnu.stud;

import edu.ntnu.stud.view.Menu;

/**
 * <STRONG>Train Dispatch Application</STRONG><br>
 * This application is a simplified version of a train dispatch system.<br>
 * The application provides the user with a menu
 * of options for managing train dispatch information.<br>
 * <p>
 * Author: Hans Lawrence Pettersen.<br>
 * Version: 5.0
 * </p>
 */
public class TrainDispatchApp {

  /**
   * Main entry point for the application.<br>
   * Creates an instance of the Menu class,
   * initializes the application components, and starts the main menu loop.
   */
  public static void main(String[] args) {
    Menu menu = new Menu();
    menu.init();
    menu.start();
  }
}