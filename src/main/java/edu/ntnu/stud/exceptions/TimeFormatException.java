package edu.ntnu.stud.exceptions;

/**
 * Thrown if the time string has an invalid format or exceeds the duration (00:00 - 24:00).
 * <p>
 *   Extends {@link java.time.format.DateTimeParseException} by including 24 hour duration check
 *   and a custom message: {@link TimeFormatException#getMessage()}.
 * </p>
 */
public class TimeFormatException extends Exception {

  /**
   * Message returned if {@link java.time.format.DateTimeParseException} is thrown
   * or if {@link java.time.Duration} exceeds 24 hours.
   *
   * @return String: "Invalid input: invalid time format."
   */
  @Override
  public String getMessage() {
    return "Invalid input: invalid time format.";
  }
}