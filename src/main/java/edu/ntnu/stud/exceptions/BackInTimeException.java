package edu.ntnu.stud.exceptions;

/**
 * Thrown if the new system time is before
 * or equal to the current system time.<br>
 * Provides a custom message: {@link BackInTimeException#getMessage()}.
 */
public class BackInTimeException extends Throwable {

  /**
   * Message returned if the updated system time is before
   * or equal to the current system time.
   *
   * @return String: "Invalid input: updated time must be later than current time."
   */
  @Override
  public String getMessage() {
    return "Invalid input: updated time must be later than current time.";
  }
}
