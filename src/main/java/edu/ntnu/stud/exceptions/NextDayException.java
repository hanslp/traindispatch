package edu.ntnu.stud.exceptions;

/**
 * Thrown if the sum of the new delay and departure time exceeds the duration (00:00 - 24:00).<br>
 * Provides a custom message: {@link NextDayException#getMessage()}.
 */
public class NextDayException extends Throwable {

  /**
   * Message returned if the sum of the new delay and
   * departure time exceeds the duration (00:00 - 24:00).
   *
   * @return String: "Invalid input: departure time and delay time exceeds 24 hours."
   */
  @Override
  public String getMessage() {
    return "Invalid input: departure time and delay time exceeds 24 hours.";
  }
}
