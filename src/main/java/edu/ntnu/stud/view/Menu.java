package edu.ntnu.stud.view;

import edu.ntnu.stud.controller.TrainController;
import edu.ntnu.stud.models.Time;
import edu.ntnu.stud.models.TrainRegister;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Responsible for managing the main menu of the train dispatch application. Provides options for:
 * <ul>
 *     <li>Overview of train dispatches</li>
 *     <li>Adding new train</li>
 *     <li>Allocating track to train</li>
 *     <li>Allocating delay to train</li>
 *     <li>Searching for expected departure using train ID</li>
 *     <li>Searching for expected departure using destination</li>
 *     <li>Deleting train from list</li>
 *     <li>Updating time</li>
 * </ul>
 */
public class Menu {
  private final Scanner scanner = new Scanner(System.in);
  private TrainController trainController;

  /**
   * Initializes components of the train dispatch application:.
   * <ul>
   *     <li>{@link Time}</li>
   *     <li>{@link TrainRegister}</li>
   *     <li>{@link TrainController}</li>
   * </ul>
   * Also adds dummy data for testing.
   */
  public void init() {
    Time time = new Time();
    TrainRegister trainRegister = new TrainRegister();
    trainController = new TrainController(time, trainRegister);

    // Dummy data for testing
    trainRegister.addTrainToList("17:00", "L1", 1, "Oslo", -1, "00:00");
    trainRegister.addTrainToList("16:00", "L2", 2, "Oslo", 2, "00:05");
    trainRegister.addTrainToList("16:59", "L3", 3, "Trondheim", 3, "00:02");
  }

  /**
   * Starts the main menu loop.
   * <p>
   *   The loop will continue until the user chooses to exit the program.
   * </p>
   */
  public void start() {
    int choice;
    boolean exit = false;

    while (!exit) {
      System.out.println("\nMain Menu:");
      System.out.println("1. Show train overview");
      System.out.println("2. Add new train");
      System.out.println("3. Allocate track to train");
      System.out.println("4. Allocate delay to train");
      System.out.println("5. Search for expected departure using train ID");
      System.out.println("6. Search for expected departure using destination");
      System.out.println("7. Delete train from list");
      System.out.println("8. Update time");
      System.out.println("9. Exit");
      System.out.print("Enter your choice: ");

      try {
        choice = scanner.nextInt();
        scanner.nextLine();

        switch (choice) {
          // print overview
          case 1:
            trainController.getOverview();
            break;

          // add new train
          case 2:
            trainController.addTrain(scanner);
            break;

          //allocate track
          case 3:
            trainController.setTrackByTrainId(scanner);
            break;

          //set delay
          case 4:
            trainController.setDelayByTrainId(scanner);
            break;

          //find departure through train ID
          case 5:
            trainController.getDepartureByTrainId(scanner);
            break;

          //find departure through destination
          case 6:
            trainController.getDepartureByDestination(scanner);
            break;

          // delete train
          case 7:
            trainController.deleteTrainByTrainId(scanner);
            break;

          //update time
          case 8:
            trainController.updateTime(scanner);
            break;

          // exit
          case 9:
            exit = true;
            System.out.println("\nExiting program.");
            break;
          default:
            System.out.println("Invalid input: please enter a valid choice.");
        }
      } catch (InputMismatchException e) {
        System.out.println("Invalid input: please enter a valid choice.");
        scanner.next();
      }
    }
  }
}