package edu.ntnu.stud.controller;

import edu.ntnu.stud.models.TrainRegister;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Functionality test of the methods in the {@link InputValidator} class.
 * <ul>
 *   <li>{@link InputValidator#validateString(Scanner, String, boolean)}</li>
 *   <li>{@link InputValidator#validateTrain(Scanner, String, boolean)}</li>
 *   <li>{@link InputValidator#validateTrack(Scanner, String)}</li>
 * </ul>
 *
 * @see TrainRegister
 */
public class InputValidatorTest {
  private InputValidator inputValidator;
  private TrainRegister trainRegister;
  private Scanner scanner;

  /**
   * Helper method for simulating scanner input.
   *
   * @param input The input string to be simulated.
   * @return A Scanner object with simulated input.
   */
  private Scanner simScan(String input) {
    InputStream in = new ByteArrayInputStream(input.getBytes());
    return new Scanner(in);
  }

  /**
   * Helper method for capturing printed output.
   *
   * @param action Performed for capturing output.
   * @return Captured output as a string.
   */
  private static String capOutput(Runnable action) {
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    System.setOut(new PrintStream(outputStream));
    action.run();
    System.setOut(System.out);
    return outputStream.toString().trim();
  }

  /**
   * Ensures clean and consistent state before of each test. Creates new
   * instance of {@link TrainRegister} and {@link InputValidator} for every test.
   */
  @BeforeEach
  void setUp() {
    trainRegister = new TrainRegister();
    inputValidator = new InputValidator(trainRegister);
  }

  /**
   * Ensures clean and consistent state after each test.
   */
  @AfterEach
  void tearDown() {
    System.setIn(System.in);
  }

  //validateString
  /**
   * Tests the {@link InputValidator#validateString(Scanner, String, boolean)} method with valid input, expecting
   * it to return the input.
   */
  @Test
  void validateString_ValidInput_ReturnsInput() {
    scanner = simScan("validInput\n");

    String result = inputValidator.validateString(scanner, "Enter input: ", false);

    assertEquals("validInput", result);
  }

  /**
   * Tests the {@link InputValidator#validateString(Scanner, String, boolean)} method with a return command,
   * expecting it to return minus two.
   */
  @Test
  void validateString_ReturnCommand_ReturnsMinusTwo() {
    scanner = simScan("return\n");

    String result = inputValidator.validateString(scanner, "Enter input: ", false);

    assertEquals("-2", result);
  }

  /**
   * Tests the {@link InputValidator#validateString(Scanner, String, boolean)} method with a return command
   * while validating time, expecting it to return minus two.
   */
  @Test
  void validateString_ReturnCommandWhileValidatingTime_ReturnsMinusTwo() {
    scanner = simScan("return\n");

    String result = inputValidator.validateString(scanner, "Enter time: ", true);

    assertEquals("-2", result);
  }

  /**
   * Tests the {@link InputValidator#validateString(Scanner, String, boolean)} method with a return command,
   * expecting it NOT to print a time format exception message.
   */
  @Test
  void validateString_ReturnCommandWhileValidatingTime_NotPrintExceptionMessage() {
    scanner = simScan("return\n");

    //helper method to capture printed output
    String printedOutput = capOutput(() -> inputValidator.validateString(scanner, "Enter time: ", true));

    // TimeFormatException message is not printed
    assertFalse(printedOutput.contains("Invalid input: invalid time format."));
  }

  /**
   * Tests the {@link InputValidator#validateString(Scanner, String, boolean)} method with valid time input,
   * expecting it to return the input.
   */
  @Test
  void validateString_ValidTimeInput_ReturnsInput() {
    scanner = simScan("10:30\n");

    String result = inputValidator.validateString(scanner, "Enter time: ", true);

    assertEquals("10:30", result);
  }

  /**
   * Tests the {@link InputValidator#validateString(Scanner, String, boolean)} method with invalid time input,
   * expecting it to print a time format exception message.
   */
  @Test
  void validateString_InvalidTimeInput_PrintsTimeFormatExceptionMessage() {
    scanner = simScan("not(hh:mm)format\nreturn\n");

    //helper method to capture printed output
    String printedOutput = capOutput(() -> inputValidator.validateString(scanner, "Enter time: ", true));

    // TimeFormatException message is printed
    assertTrue(printedOutput.contains("Invalid input: invalid time format."));
  }

  /**
   * Tests the {@link InputValidator#validateString(Scanner, String, boolean)} method with invalid time input,
   * expecting it to loop until valid input is given.
   */
  @Test
  void validateString_InvalidTimeInputLoop_LoopsUntilValid() {
    scanner = simScan("invalidTime\ninvalidTime\n10:30\n");

    String result = inputValidator.validateString(scanner, "Enter time: ", true);

    assertEquals("10:30", result);
  }

  /**
   * Tests the {@link InputValidator#validateString(Scanner, String, boolean)} method with empty input,
   * expecting it to print an empty input message.
   */
  @Test
  void validateString_EmptyInput_PrintsEmptyMessage() {
    scanner = simScan("\n\n\nreturn\n");

    //helper method to capture printed output
    String printedOutput = capOutput(() -> inputValidator.validateString(scanner, "Enter input: ", false));

    assertTrue(printedOutput.contains("Invalid input: empty input."));
  }

  /**
   * Tests the {@link InputValidator#validateString(Scanner, String, boolean)} method with empty input,
   * expecting it to loop until valid input is given.
   */
  @Test
  void validateString_EmptyInputLoop_LoopsUntilValidInput() {
    scanner = simScan("\n\nvalidInput\n");

    String result = inputValidator.validateString(scanner, "Enter input: ", false);

    assertEquals("validInput", result);
  }

  //validateTrain
  /**
   * Tests the {@link InputValidator#validateTrain(Scanner, String, boolean)} method with valid integer input,
   * expecting it to return the input.
   */
  @Test
  void validateTrain_ValidIntegerInput_ReturnsInput() {
    scanner = simScan("20\n");

    int result = inputValidator.validateTrain(scanner, "Enter train ID: ", false);

    assertEquals(20, result);
  }

  /**
   * Tests the {@link InputValidator#validateTrain(Scanner, String, boolean)} method with a return command, expecting
   * it to return minus two.
   */
  @Test
  void validateTrain_ReturnCommand_ReturnsMinusTwo() {
    scanner = simScan("return\n");

    int result = inputValidator.validateTrain(scanner, "Enter train ID: ", false);

    assertEquals(-2, result);
  }

  /**
   * Tests the {@link InputValidator#validateTrain(Scanner, String, boolean)} method with invalid integer input,
   * expecting it to print integer exception message.
   */
  @Test
  void validateTrain_InvalidIntegerInput_PrintsIntegerMessage() {
    scanner = simScan("notAnInteger\nreturn\n");

    //helper method to capture printed output
    String printedOutput = capOutput(() -> inputValidator.validateTrain(scanner, "Enter train ID: ", false));

    assertTrue(printedOutput.contains("Invalid input: not an integer."));
  }

  /**
   * Tests the {@link InputValidator#validateTrain(Scanner, String, boolean)} method with two invalid integer inputs,
   * expecting it to loop until valid input is given in the third input.
   */
  @Test
  void validateTrain_InvalidInputLoop_LoopsUntilValid() {
    scanner = simScan("notAnInteger\nnotAnInteger\n20\n");

    int result = inputValidator.validateTrain(scanner, "Enter train ID: ", false);

    assertEquals(20, result);
  }

  /**
   * Tests the {@link InputValidator#validateTrain(Scanner, String, boolean)} method while validateTrainId is true,
   * with a not unique train ID as input, expecting it to print a not unique train ID message.
   * It is arranged that a train is added to the train register with train Id (1).
   * Then we simulate user input with train Id (1) and a return command.
   * We capture the printed output and assert that it contains the not unique train ID message.
   */
  @Test
  void validateTrain_NotUniqueTrainID_PrintsNotUniqueMessage() {
    trainRegister.addTrainToList("17:00", "L1", 1, "Oslo", -1, "00:00");
    scanner = simScan("1\nreturn\n");

    //helper method to capture printed output
    String printedOutput = capOutput(() -> inputValidator.validateTrain(scanner, "Enter train ID: ", true));

    assertTrue(printedOutput.contains("Invalid input: Train ID already in use."));
  }

  /**
   * Tests the {@link InputValidator#validateTrain(Scanner, String, boolean)} method while validateTrainId is true,
   * with a not unique train ID as input two times, expecting it to loop until valid input is given in the third input.
   * It is arranged that a train is added to the train register with train Id (1).
   * Then we simulate user input with train Id (1) twice and then train Id (20).
   * We assert that the third input is returned.
   */
  @Test
  void validateTrain_NotUniqueTrainIDLoop_LoopsUntilUnique() {
    scanner = simScan("1\n1\n20\n");
    trainRegister.addTrainToList("17:00", "L1", 1, "Oslo", -1, "00:00");

    int result = inputValidator.validateTrain(scanner, "Enter train ID: ", true);

    assertEquals(20, result);
  }

  //validateTrack
  /**
   * Tests the {@link InputValidator#validateTrack(Scanner, String)} method with valid integer input,
   * expecting it to return the input.
   */
  @Test
  void validateTrack_ValidInput_ReturnsInput() {
    scanner = simScan("69\n");

    int result = inputValidator.validateTrack(scanner, "Enter track: ");

    assertEquals(69, result);
  }

  /**
   * Tests the {@link InputValidator#validateTrack(Scanner, String)} method with empty input,
   * expecting it to return minus one (indicating that a track is not yet determined)
   */
  @Test
  void validateTrack_EmptyInput_ReturnsMinusOne() {
    scanner = simScan("\n");

    int result = inputValidator.validateTrack(scanner, "Enter track: ");

    assertEquals(-1, result);
  }

  /**
   * Tests the {@link InputValidator#validateTrack(Scanner, String)} method with a return command,
   * expecting it to return minus two.
   */
  @Test
  void validateTrack_ReturnCommand_ReturnsMinusTwo() {
    scanner = simScan("return\n");

    int result = inputValidator.validateTrack(scanner, "Enter track: ");

    assertEquals(-2, result);
  }

  /**
   * Tests the {@link InputValidator#validateTrack(Scanner, String)} method with invalid input,
   * expecting it to print integer exception message.
   */
  @Test
  void validateTrack_InvalidInput_PrintsInvalidMessage() {
    scanner = simScan("notAnInteger\nreturn\n");

    //helper method to capture printed output
    String printedOutput = capOutput(() -> inputValidator.validateTrack(scanner, "Enter track: "));

    assertTrue(printedOutput.contains("Invalid input: not an integer."));
  }

  /**
   * Tests the {@link InputValidator#validateTrack(Scanner, String)} method with two invalid integer inputs,
   * expecting it to loop until valid input is given in the third input.
   */
  @Test
  void validateTrack_InvalidInputLoop_LoopsUntilValid() {
    scanner = simScan("notAnInteger\nnotAnInteger\n69\n");

    int result = inputValidator.validateTrack(scanner, "Enter track:");

    assertEquals(69, result);
  }
}