package edu.ntnu.stud.controller;

import edu.ntnu.stud.exceptions.BackInTimeException;
import edu.ntnu.stud.exceptions.TimeFormatException;
import edu.ntnu.stud.exceptions.NextDayException;
import edu.ntnu.stud.models.TrainRegister;
import edu.ntnu.stud.models.Time;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.time.LocalTime;
import java.util.Scanner;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * Functionality test of the methods in the {@link TrainController} class:
 * <ul>
 *   <li>{@link TrainController#getOverview()}</li>
 *   <li>{@link TrainController#addTrain(Scanner)}</li>
 *   <li>{@link TrainController#setTrackByTrainId(Scanner)}</li>
 *   <li>{@link TrainController#setDelayByTrainId(Scanner)}</li>
 *   <li>{@link TrainController#getDepartureByTrainId(Scanner)}</li>
 *   <li>{@link TrainController#getDepartureByDestination(Scanner)}</li>
 *   <li>{@link TrainController#deleteTrainByTrainId(Scanner)}</li>
 *   <li>{@link TrainController#updateTime(Scanner)}</li>
 * </ul>
 * Also tests simulated scenarios in the user interface.
 * @see NextDayException
 * @see TimeFormatException
 */
public class TrainControllerTest {
  private Scanner scanner;
  private Time time;
  private TrainRegister trainRegister;
  private InputValidator inputValidator;

  /**
   * Helper method for simulating scanner input.
   *
   * @param input The input string to be simulated.
   * @return A Scanner object with simulated input.
   */
  private Scanner simScan(String input) {
    InputStream in = new ByteArrayInputStream(input.getBytes());
    return new Scanner(in);
  }

  /**
   * Helper method for capturing printed output.
   *
   * @param action Performed for capturing output.
   * @return Captured output as a string.
   */
  private static String capOutput(Runnable action) {
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    System.setOut(new PrintStream(outputStream));
    action.run();
    System.setOut(System.out);
    return outputStream.toString().trim();
  }

  /**
   * Sets up a clean and consistent state before each test. Creates new instance of:
   * <ul>
   *   <li>{@link Time}</li>
   *   <li>{@link TrainRegister}</li>
   *   <li>{@link InputValidator}</li>
   *   <li><STRONG>Train</STRONG> through: {@link TrainRegister#addTrainToList(String, String, int, String, int, String)}</li>
   * </ul>
   */
  @BeforeEach
  void setUp() {
    time = new Time();
    trainRegister = new TrainRegister();
    inputValidator = new InputValidator(trainRegister);
    trainRegister.addTrainToList("12:00", "L1", 1, "Oslo", 1, "00:00");

  }

  /**
   * Ensures clean and consistent state after each test.
   */
  @AfterEach
  void tearDown() {
    System.setIn(System.in);
  }

  // getOverview
  /**
   * Tests {@link TrainController#getOverview()} expecting the printed output to contain
   * the only train in the train list.
   */
  @Test
  void getOverview_NotDeparted_PrintsNotDeparted() {
    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.getOverview();
    });

    assertTrue(printedOutput.contains("| 1        | Oslo             | 12:00     | On time  | L1    | 1     |"));
  }

  /**
   * Tests {@link TrainController#getOverview()} after having updated the time to 12:01 (after the expected departure
   * of the dummy train) expecting the printed output to NOT contain the dummy train.
   */
  @Test
  void getOverview_Departed_PrintsNotDeparted() throws BackInTimeException {
    // Set time to 12:01
    time.setSystemTime("12:01");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.getOverview();
    });

    assertFalse(printedOutput.contains("| 1        | Oslo             | 12:00     | On time  | L1    | 1     |"));
  }

  /**
   * Tests {@link TrainController#getOverview()} in the scenario where there is one train that has departed and one train
   * that has not departed. Expects the printed output to only contain the train that has not departed.
   */
  @Test
  void getOverview_MixedDeparture_PrintsNotDeparted() throws BackInTimeException {
    trainRegister.addTrainToList("13:00", "L2", 2, "Oslo", 1, "00:00");
    // Set time to 12:01
    time.setSystemTime("12:01");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.getOverview();
    });

    assertFalse(printedOutput.contains(
        "| 1        | Oslo             | 12:00     | On time  | L1    | 1     |"));
    assertTrue(printedOutput.contains(
        "| 2        | Oslo             | 13:00     | On time  | L2    | 1     |"));
  }

  /**
   * Tests that {@link TrainController#getOverview()} prints On Time in the expected departure column,
   * if the train has no delay (00:00).
   */
  @Test
  void getOverview_OnTime_PrintsOnTime() {
    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.getOverview();
    });

    assertFalse(printedOutput.contains(
        "| 1        | Oslo             | 12:00     | 12:00    | L1    | 1     |"));
    assertTrue(printedOutput.contains(
        "| 1        | Oslo             | 12:00     | On time  | L1    | 1     |"));
  }

  /**
   * Tests that {@link TrainController#getOverview()} prints the expected departure time (delay+departure)
   * in the expected departure column,
   */
  @Test
  void getOverview_NotOnTime_PrintsExpectedDeparture() {
    trainRegister.addTrainToList("13:00", "L2", 2, "Oslo", 1, "00:37");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.getOverview();
    });

    assertFalse(printedOutput.contains(
        "| 2        | Oslo             | 13:00     | On time  | L1    | 1     |"));
    assertTrue(printedOutput.contains(
        "| 2        | Oslo             | 13:00     | 13:37    | L2    | 1     |"));
  }

  /**
   * Tests that {@link TrainController#getOverview()} prints TBD in the track column,
   * if the train has track number (-1)
   */
  @Test
  void getOverview_TrackMinusOne_PrintsTBD() {
    trainRegister.addTrainToList("13:00", "L2", 2, "Oslo", -1, "00:00");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.getOverview();
    });

    assertFalse(printedOutput.contains(
        "| 2        | Oslo             | 13:00     | On time  | L1    | -1    |"));
    assertTrue(printedOutput.contains(
        "| 2        | Oslo             | 13:00     | On time  | L2    | TBD   |"));
  }

  //addTrain
  /**
   * Tests {@link TrainController#addTrain(Scanner)} with valid input, expecting the train list size to increase by 1.
   */
  @Test
  void addTrain_ValidInput_AddsToList() {
    TrainController trainController = new TrainController(time, trainRegister);
    int priorSize = trainRegister.getSortedTrainList().size();
    scanner = simScan("13:37\nL2\n69\nDestination\n2\n");

    trainController.addTrain(scanner);
    int postSize = trainRegister.getSortedTrainList().size();

    assertEquals(priorSize + 1, postSize, "Train list size should be 2");
  }

  @Test
  void addTrain_GetParametersOfAddedTrain_ReturnsCorrectParameters() {
    scanner = simScan("13:37\nL2\n69\nOslo\n2\n");
    TrainController trainController = new TrainController(time, trainRegister);

    trainController.addTrain(scanner);

    assertEquals("13:37", trainRegister.getSortedTrainList().get(1).getDepartureTime(), "Departure time should be 13:37");
    assertEquals("L2", trainRegister.getSortedTrainList().get(1).getLine(), "Line should be L2");
    assertEquals(69, trainRegister.getSortedTrainList().get(1).getTrainId(), "Train ID should be 69");
    assertEquals("Oslo", trainRegister.getSortedTrainList().get(1).getDestination(), "Destination should be Oslo");
    assertEquals(2, trainRegister.getSortedTrainList().get(1).getTrack(), "Track should be 2");
  }

  /**
   * Tests {@link TrainController#addTrain(Scanner)} with empty input for the departure time parameter,
   * expecting the empty input message to be printed
   */
  @Test
  void addTrain_EmptyInput_PrintEmptyMessage() {
    scanner = simScan("\nreturn\n");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController1 = new TrainController(time, trainRegister);
      trainController1.addTrain(scanner);
    });

    assertTrue(printedOutput.contains("Invalid input: empty input."));
  }

  /**
   * Tests {@link TrainController#addTrain(Scanner)} with empty input for the track parameter,
   * expecting the track to be set to -1.
   * <p> The simulated input sequence is as follows:
   * <ol>
   *   <li>13:37: departure time</li>
   *   <li>L2: line</li>
   *   <li>69: train ID</li>
   *   <li>Destination: destination</li>
   *   <li>(empty input): track</li>
   * </ol>
   * </p>
   */
  @Test
  void addTrain_EmptyTrackInput_TrackMinusOne() {
    scanner = simScan("13:37\nL2\n69\nDestination\n\n");
    TrainController trainController = new TrainController(time, trainRegister);

    trainController.addTrain(scanner);

    assertEquals(-1, trainRegister.getSortedTrainList().get(1).getTrack(), "Track should be -1");
  }

  /**
   * Tests {@link TrainController#addTrain(Scanner)} with an invalid time format for departure time parameter,
   * expects {@link TimeFormatException#getMessage} to be printed
   */
  @Test
  void addTrain_InvalidTimeInputCatchesException_PrintTimeFormatMessage() {
    scanner = simScan("not(hh:mm)format\nreturn\n");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController1 = new TrainController(time, trainRegister);
      trainController1.addTrain(scanner);
    });

    // Exception message from TimeFormatException
    assertTrue(printedOutput.contains("Invalid input: invalid time format."));
  }

  /**
   * Tests {@link TrainController#addTrain(Scanner)} with a non-unique train ID, expects non-unique train ID message
   * to be printed, expects the train list size to remain unchanged.
   * <p>
   *   The simulated input sequence is as follows:
   *   <ol>
   *     <li>13:37: departure time</li>
   *     <li>L1: line</li>
   *     <li>1: train ID</li>
   *     <li>Oslo: train ID</li>
   *     <li>1: train ID</li>
   *     <li>return: return command(due to input sequence being halted at 3. train ID))</li>
   * </p>
   */
  @Test
  void addTrain_TrainNumberNotUnique_TrainNotAddedPrintsError() {
    scanner = simScan("13:37\nL1\n1\nOslo\n1\nreturn\n");

    int priorSize = trainRegister.getSortedTrainList().size();
    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController1 = new TrainController(time, trainRegister);
      trainController1.addTrain(scanner);
    });
    int postSize = trainRegister.getSortedTrainList().size();

    assertTrue(printedOutput.contains("Invalid input: Train ID already in use."));
    assertEquals(priorSize, postSize);
  }

  /**
   * Tests {@link TrainController#addTrain(Scanner)} with an invalid input at the first parameter.
   * Expects the invalid input message to be printed, expects no other parameter prompts to be printed.
   * <p>
   *   The simulated input sequence is as follows:
   *   <ol>
   *     <li>not(hh:mm)format: departure time</li>
   *     <li>Line: departure time</li>
   *     <li>69: departure time</li>
   *     <li>Destination: departure time</li>
   *     <li>2: departure time</li>
   *     <li>return: return command(due to input sequence being halted at 1. departure time))</li>
   *  </ol>
   *  </p>
   */
  @Test
  void addTrain_InvalidInputLoop_StopsProgressionLoopsAtCurrent() {
    scanner = simScan("not(hh:mm)format\nLine\n69\nDestination\n2\nreturn\n");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.addTrain(scanner);
    });

    assertTrue(printedOutput.contains("Invalid input: invalid time format."));
    assertFalse(printedOutput.contains("Enter line: "));
    assertFalse(printedOutput.contains("Enter train ID: "));
    assertFalse(printedOutput.contains("Enter destination: "));
    assertFalse(printedOutput.contains("Enter track: "));
  }

  /**
   * Tests {@link TrainController#addTrain(Scanner)} with the return command at the first parameter,
   * expects the train list size to remain unchanged.
   */
  @Test
  void addTrain_ReturnCommand_NoTrainAdded() {
    int priorSize = trainRegister.getSortedTrainList().size();
    scanner = simScan("return\n13:37\nL2\n69\nDestination\n2\n");
    TrainController trainController = new TrainController(time, trainRegister);

    trainController.addTrain(scanner);
    int postSize = trainRegister.getSortedTrainList().size();

    assertEquals(priorSize, postSize, "Train list size should be 1 not 2");
  }

    /**
     * Tests {@link TrainController#addTrain(Scanner)} with the return command at the second parameter,
     * expects the train list size to remain unchanged.
     */
  @Test
  void addTrain_ReturnCommand2ndPrompt_NoTrainAdded() {
    scanner = simScan("13:37\nreturn\n69\nDestination\n2\n");
    TrainController trainController = new TrainController(time, trainRegister);

    trainController.addTrain(scanner);

    assertEquals(1, trainRegister.getSortedTrainList().size(), "Train list size should be 1 not 2");
  }

  @Test
  void addTrain_ReturnCommand3rdPrompt_NoTrainAdded() {
    scanner = simScan("13:37\nL2\nreturn\nDestination\n2\n");
    TrainController trainController = new TrainController(time, trainRegister);

    trainController.addTrain(scanner);

    assertEquals(1, trainRegister.getSortedTrainList().size(), "Train list size should be 1 not 2");
  }

  @Test
  void addTrain_ReturnCommand4thPrompt_NoTrainAdded() {
    scanner = simScan("13:37\nL2\n69\nreturn\n2\n");
    TrainController trainController = new TrainController(time, trainRegister);

    trainController.addTrain(scanner);

    assertEquals(1, trainRegister.getSortedTrainList().size(), "Train list size should be 1 not 2");
  }

  @Test
  void addTrain_ReturnCommand5thPrompt_NoTrainAdded() {
    scanner = simScan("13:37\nL2\n69\nDestination\nreturn\n");
    TrainController trainController = new TrainController(time, trainRegister);

    trainController.addTrain(scanner);

    assertEquals(1, trainRegister.getSortedTrainList().size(), "Train list size should be 1 not 2");
  }

  //setTrackByTrainID
  @Test
  void setTrackByTrainID_ValidInput_SetsTrack() {
    // first input identifies train ID, second input refers to track
    scanner = simScan("1\n2\n");
    TrainController trainController = new TrainController(time, trainRegister);

    trainController.setTrackByTrainId(scanner);

    assertEquals(2, trainRegister.getSortedTrainList().get(0).getTrack(), "Track should be 2");
  }

  @Test
  void setTrackByTrainID_ReturnCommand_TrackNotSet() {
    // first input identifies train ID, second input refers to track
    scanner = simScan("return\n1\n2\n");
    TrainController trainController = new TrainController(time, trainRegister);

    trainController.setTrackByTrainId(scanner);

    assertEquals(1, trainRegister.getSortedTrainList().get(0).getTrack(), "Track should be 1");
  }

  @Test
  void setTrackByTrainID_ReturnCommand2ndPrompt_TrackNotSet() {
    // first input identifies train ID, second input refers to track
    scanner = simScan("1\nreturn\n2\n");
    TrainController trainController = new TrainController(time, trainRegister);

    trainController.setTrackByTrainId(scanner);

    assertEquals(1, trainRegister.getSortedTrainList().get(0).getTrack(), "Track should be 1");
  }

  /**
   * Tests {@link TrainController#setTrackByTrainId(Scanner)} with an empty input for the track parameter,
   * expects the track to be set to -1.
   */
  @Test
  void setTrackByTrainID_EmptyInput_TrackMinusOne() {
    // first input identifies train ID, second input refers to track
    scanner = simScan("1\n\n");
    TrainController trainController = new TrainController(time, trainRegister);

    trainController.setTrackByTrainId(scanner);

    assertEquals(-1, trainRegister.getSortedTrainList().get(0).getTrack(), "Track should be -1");
  }

  /**
   * Tests {@link TrainController#setTrackByTrainId(Scanner)} with an invalid input for the track parameter,
   * expects the track to remain unchanged until a valid input is provided.
   */
  @Test
  void setTrackByTrainID_InvalidInput2ndPrompt_Loops2ndPromptUntilValid() {
    // first input identifies train ID, second input refers to track
    scanner = simScan("1\nnotAnInteger\n15\n");
    TrainController trainController = new TrainController(time, trainRegister);

    trainController.setTrackByTrainId(scanner);

    assertEquals(15, trainRegister.getSortedTrainList().get(0).getTrack(), "Track should be 15");
  }

  //setDelayByTrainID

  /**
   * Tests {@link TrainController#setDelayByTrainId(Scanner)} with valid input for the delay parameter,
   * expects the delay to be set to the provided value.
   */
  @Test
  void setDelayByTrainID_ValidInput_SetsDelay() {
    // first input identifies train ID, second input refers to delay
    scanner = simScan("1\n01:30\n");
    TrainController trainController = new TrainController(time, trainRegister);

    trainController.setDelayByTrainId(scanner);

    assertNotEquals("00:00", trainRegister.getSortedTrainList().get(0).getDelay(), "Delay should not be 00:00");
    assertEquals("01:30", trainRegister.getSortedTrainList().get(0).getDelay(), "Delay should be 01:30");
  }

  /**
   * Tests {@link TrainController#setDelayByTrainId(Scanner)} with valid input for the delay parameter,
   * expects the printed output to contain the train ID and the new delay.
   */
  @Test
  void setDelayByTrainID_ValidInput_PrintsAddedDelay() {
    // first input identifies train ID, second input refers to delay
    scanner = simScan("1\n01:30\n");

    // use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.setDelayByTrainId(scanner);
    });

    assertTrue(printedOutput.contains("Train ID: 1, new delay: 01:30"));
  }

  /**
   * Tests {@link TrainController#setDelayByTrainId(Scanner)} with an invalid train Id at the first parameter,
   * expects the delay to remain unchanged.
   */
  @Test
  void setDelayByTrainID_InvalidTrainID_DelayNotSet() {
    // first input identifies train ID, second input refers to delay
    scanner = simScan("999\n01:30\nreturn\n");

    // use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.setDelayByTrainId(scanner);
    });

    assertFalse(printedOutput.contains("Train ID: 999, new delay: 01:30"));
  }

  /**
   * Tests {@link TrainController#setDelayByTrainId(Scanner)} with an invalid train Id at the first parameter,
   * expects the printed output to contain train not found message
   */
  @Test
  void setDelayByTrainID_InvalidTrainID_PrintsNotFoundMessage() {
    // first input identifies train ID, second input refers to delay
    scanner = simScan("999\n01:30\nreturn\n");

    // use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.setDelayByTrainId(scanner);
    });

    assertTrue(printedOutput.contains("Train ID: 999 not found."));
  }

  /**
   * Tests {@link TrainController#setDelayByTrainId(Scanner)} with an invalid input for the delay parameter,
   * expects the delay to remain unchanged and {@link TimeFormatException#getMessage()} to be printed.
   */
  @Test
  void setDelayByTrainID_InvalidTimeFormat_DelayNotSet_PrintsErrorMessage() {
    // first input identifies train ID, second input refers to delay
    scanner = simScan("1\nnot(hh:mm)format\nreturn\n");
    String initialDelay = trainRegister.getSortedTrainList().get(0).getDelay();
    // use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.setDelayByTrainId(scanner);
    });
    assertEquals(initialDelay, trainRegister.getSortedTrainList().get(0).getDelay(), "Delay should be 00:00");
    assertTrue(printedOutput.contains("Invalid input: invalid time format."));
  }

  /**
   * Tests {@link TrainController#setDelayByTrainId(Scanner)} with an invalid input for the delay parameter,
   * expects the delay to remain unchanged until a valid input is provided.
   * <p>
   *   The simulated input sequence is as follows:
   *   <ol>
   *     <li>1: train ID</li>
   *     <li>not(hh:mm)format: delay</li>
   *     <li>not(hh:mm)format: delay</li>
   *     <li>01:30: delay</li>
   *   </ol
   * </p>
   */
  @Test
  void setDelayByTrainID_InvalidInput2ndPrompt_Loops2ndPromptUntilValid() {
    // first input identifies train ID, second input refers to delay
    scanner = simScan("1\ninvalid\ninvalid\n01:30\n");
    TrainController trainController = new TrainController(time, trainRegister);

    trainController.setDelayByTrainId(scanner);

    assertEquals("01:30", trainRegister.getSortedTrainList().get(0).getDelay(), "Delay should be 01:30");
  }

  @Test
  void setDelayByTrainID_ReturnCommand_DelayNotSet() {
    // first input identifies train ID, second input refers to delay
    scanner = simScan("return\n1\n01:30\n");
    TrainController trainController = new TrainController(time, trainRegister);

    trainController.setDelayByTrainId(scanner);

    assertNotEquals("01:30", trainRegister.getSortedTrainList().get(0).getDelay(), "Delay should be 00:00");
  }

  @Test
  void setDelayByTrainID_ReturnCommand2ndPrompt_DelayNotSet() {
    // first input identifies train ID, second input refers to delay
    scanner = simScan("1\nreturn\n01:30\n");
    TrainController trainController = new TrainController(time, trainRegister);

    trainController.setDelayByTrainId(scanner);

    assertNotEquals("01:30", trainRegister.getSortedTrainList().get(0).getDelay(), "Delay should be 00:00");
  }

  /**
   * Tests {@link TrainController#setDelayByTrainId(Scanner)} in the scenario where the new delay+departure time exceeds
   * 24 hours. Expects delay NOT to be set, and {@link NextDayException#getMessage()} to be printed.
   */
  @Test
  void setDelayByTrainID_CatchesNextDayException_DelayNotSet_PrintsExceptionMessage() {
    // first input identifies train ID, second input refers to delay.
    scanner = simScan("1\n23:00\nreturn\n");
    trainRegister.addTrainToList("12:00", "L1", 1, "Oslo", 1, "00:00");

    // use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.setDelayByTrainId(scanner);
    });

    // check that the printed output contains the message from NextDayException
    assertTrue(printedOutput.contains("Invalid input: departure time and delay time exceeds 24 hours."));
    assertNotEquals("23:00", trainRegister.getSortedTrainList().get(0).getDelay(), "Delay should be 00:00");
  }

  // departureByTrainID
  /**
   * Tests {@link TrainController#getDepartureByTrainId(Scanner)} in the scenario where the train has not departed.
   * Expects the departure of the train to be printed.
   */
  @Test
  void getDepartureByTrainID_ValidInputAfterTime_PrintsDeparture() {
    scanner = simScan("1\n");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.getDepartureByTrainId(scanner);
    });

    assertTrue(printedOutput.contains("Train ID: 1, expected departure: 12:00"));
  }

  /**
   * Tests {@link TrainController#getDepartureByTrainId(Scanner)} in the scenario where the train has already departed.
   * Expects already departed message to be printed.
   */
  @Test
  void getDepartureByTrainID_ValidInputDepartureBeforeTime_PrintsDepartedMessage() throws BackInTimeException {
    scanner = simScan("1\n");
    // Set time to 12:01
    time.setSystemTime("12:01");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.getDepartureByTrainId(scanner);
    });

    assertFalse(printedOutput.contains("Train ID: 1, expected departure: 12:00"));
    assertTrue(printedOutput.contains("Train ID: 1, already departed at: 12:00"));
  }

  @Test
  void getDepartureByTrainID_ReturnCommand_NoDeparturePrinted() {
    scanner = simScan("return\n1\n");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.getDepartureByTrainId(scanner);
    });

    assertFalse(printedOutput.contains("Train ID: 1, expected departure: 12:00"));
  }

  /**
   * Tests {@link TrainController#getDepartureByTrainId(Scanner)} with an invalid train Id at the first parameter,
   * expects the printed output to contain train not found message
   */
  @Test
  void getDepartureByTrainID_InvalidTrainID_PrintsNotFoundMessage() {
    scanner = simScan("999\nreturn\n");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.getDepartureByTrainId(scanner);
    });

    assertTrue(printedOutput.contains("Train ID: 999 not found."));
  }

  /**
   * Tests {@link TrainController#getDepartureByTrainId(Scanner)} with an invalid input for the train ID parameter,
   * expects the printed output to contain not an integer message
   */
  @Test
  void getDepartureByTrainID_InvalidInputString_PrintsIntegerMessage() {
    scanner = simScan("notAnInteger\nreturn\n");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.getDepartureByTrainId(scanner);
    });

    assertTrue(printedOutput.contains("Invalid input: not an integer."));
  }

  /**
   * Tests {@link TrainController#getDepartureByTrainId(Scanner)} with invalid inputs for the train ID parameter,
   * expects the prompt to loop until a valid input is provided.
   */
  @Test
  void getDepartureByTrainID_InvalidInputLoop_LoopsUntilValid() {
    scanner = simScan("notAnInteger\nnotAnInteger\n1\n");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.getDepartureByTrainId(scanner);
    });

    assertTrue(printedOutput.contains("Train ID: 1, expected departure: 12:00"));
  }

  // departureByDestination
  /**
   * Tests {@link TrainController#getDepartureByDestination(Scanner)} in the scenario where there is one train with a
   * matching destination. Expects the departure of the train to be printed.
   */
  @Test
  void getDepartureByDestination_ValidDestination_PrintsDeparture() {
    scanner = simScan("Oslo\n");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.getDepartureByDestination(scanner);
    });

    assertTrue(printedOutput.contains("Train ID: 1, expected departure: 12:00"));
  }

  /**
   * Tests {@link TrainController#getDepartureByDestination(Scanner)} in the scenario where there are multiple trains
   * with a matching destination, expects the departure of all trains to be printed.
   */
  @Test
  void getDepartureByDestination_MultipleTrains_PrintsDepartures() {
    scanner = simScan("Oslo\n");
    trainRegister.addTrainToList("13:00", "L2", 2, "Oslo", 1, "00:00");
    trainRegister.addTrainToList("14:00", "L3", 3, "Oslo", 1, "00:00");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.getDepartureByDestination(scanner);
    });

    assertTrue(printedOutput.contains("Train ID: 1, expected departure: 12:00"));
    assertTrue(printedOutput.contains("Train ID: 2, expected departure: 13:00"));
    assertTrue(printedOutput.contains("Train ID: 3, expected departure: 14:00"));
  }

  @Test
  void getDepartureByDestination_ReturnCommand_NoDeparturePrinted() {
    scanner = simScan("return\nOslo\n");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.getDepartureByDestination(scanner);
    });

    assertFalse(printedOutput.contains("Train ID: 1, expected departure: 12:00"));
  }

  /**
   * Tests {@link TrainController#getDepartureByDestination(Scanner)} in the scenario where a train with a
   * matching destination has already departed. Expects the departure of the train NOT to be printed.
   */
  @Test
  void getDepartureByDestination_AlreadyDeparted_NotPrintDeparted() throws BackInTimeException {
    scanner = simScan("Oslo\nreturn\n");
    // Set time to 12:01 (excludes Train ID: 1)
    time.setSystemTime("12:01");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.getDepartureByDestination(scanner);
    });

    assertFalse(printedOutput.contains("Train ID: 1, expected departure: 12:00"));
  }

  /**
   * Tests {@link TrainController#getDepartureByDestination(Scanner)} in the scenario where two trains have a matching
   * destination, but only one of them has NOT departed <STRONG>due to delay</STRONG>.
   * Expects train that has NOT departed to be printed.
   */
  @Test
  void getDepartureByDestination_DelayedDeparted_PrintsNotDeparted() throws BackInTimeException {
    scanner = simScan("Oslo\n");
    trainRegister.addTrainToList("13:00", "L2", 2, "Oslo", 1, "00:05");
    // Set time to 12:01 (excludes Train ID: 1)
    time.setSystemTime("13:01");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.getDepartureByDestination(scanner);
    });

    assertFalse(printedOutput.contains("Train ID: 1, expected departure: 12:00"));
    assertFalse(printedOutput.contains("Train ID: 2, expected departure: 13:00"));
    assertTrue(printedOutput.contains("Train ID: 2, expected departure: 13:05"));
  }

  /**
   * Tests {@link TrainController#getDepartureByDestination(Scanner)} in the scenario where there are multiple trains
   * with different destinations, expects only the departure of the trains with a matching destination to be printed.
   */
  @Test
  void getDepartureByDestination_MixedDestinationMultipleTrains_PrintsMatching() {
    scanner = simScan("Oslo\n");
    trainRegister.addTrainToList("13:00", "L2", 2, "Oslo", 1, "00:00");
    trainRegister.addTrainToList("15:00", "L3", 3, "Åsgard", 1, "00:00");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.getDepartureByDestination(scanner);
    });

    assertTrue(printedOutput.contains("Train ID: 1, expected departure: 12:00"));
    assertTrue(printedOutput.contains("Train ID: 2, expected departure: 13:00"));
    assertFalse(printedOutput.contains("Train ID: 3, expected departure: 15:00"));
  }

  /**
   * Tests {@link TrainController#getDepartureByDestination(Scanner)} in the scenario where there are multiple trains
   * with a matching destination, expects only trains that have NOT departed to be printed.
   */
  @Test
  void getDepartureByDestination_MixedDepartureMatchingDestination_PrintsNotDeparted() throws BackInTimeException {
    scanner = simScan("Oslo\n");
    trainRegister.addTrainToList("20:00", "L2", 2, "Oslo", 1, "00:00");
    trainRegister.addTrainToList("19:00", "L3", 3, "Oslo", 1, "00:00");
    time.setSystemTime("19:01");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.getDepartureByDestination(scanner);
    });

    assertFalse(printedOutput.contains("Train ID: 1, expected departure: 12:00"));
    assertFalse(printedOutput.contains("Train ID: 3, expected departure: 19:00"));
    assertTrue(printedOutput.contains("Train ID: 2, expected departure: 20:00"));
  }

  /**
   * Tests {@link TrainController#getDepartureByDestination(Scanner)} in the scenario where there are multiple trains
   * with a matching destination, whose departure time is also effected by delay. Expects only trains that have NOT
   * departed <STRONG>due to delay</STRONG> to be printed.
   */
  @Test
  void getDepartureByDestination_MixedDelayDepartureMatchingDestination_PrintsNotDeparted() throws BackInTimeException {
    scanner = simScan("Oslo\n");
    trainRegister.addTrainToList("17:00", "L2", 2, "Oslo", 1, "02:01");
    trainRegister.addTrainToList("18:00", "L3", 3, "Oslo", 1, "02:00");

    // Set time to 19:00
    time.setSystemTime("19:00");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.getDepartureByDestination(scanner);
    });

    assertFalse(printedOutput.contains("Train ID: 1, expected departure: 12:00"));
    assertFalse(printedOutput.contains("Train ID: 2, expected departure: 17:00"));
    assertFalse(printedOutput.contains("Train ID: 3, expected departure: 18:00"));
    assertTrue(printedOutput.contains("Train ID: 2, expected departure: 19:01"));
    assertTrue(printedOutput.contains("Train ID: 3, expected departure: 20:00"));
  }

  /**
   * Tests {@link TrainController#getDepartureByDestination(Scanner)} with two invalid inputs for the destination,
   * expects the prompt to loop until a valid input is provided.
   */
  @Test
  void getDepartureByDestination_NotFoundLoop_LoopsUntilFound() {
    scanner = simScan("Åsgard\nValaskjalv\nOslo\n");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.getDepartureByDestination(scanner);
    });

    assertTrue(printedOutput.contains("Train ID: 1, expected departure: 12:00"));
  }

  /**
   * Tests {@link TrainController#getDepartureByDestination(Scanner)} in the scenario where the destination is not
   * found, expects not found message to be printed.
   */
  @Test
  void getDepartureByDestination_InvalidDestination_PrintsNotFoundMessage() {
    scanner = simScan("Åsgard\nreturn\n");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.getDepartureByDestination(scanner);
    });

    assertTrue(printedOutput.contains("No trains with destination 'Åsgard' found."));
  }

  /**
   * Tests {@link TrainController#getDepartureByDestination(Scanner)} with an empty input, expects
   * the empty input message to be printed.
   */
  @Test
  void getDepartureByDestination_InvalidInputEmpty_PrintsEmptyMessage() {
    scanner = simScan("\nreturn\n");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.getDepartureByDestination(scanner);
    });

    assertFalse(printedOutput.contains("No trains with destination '' found."));
    assertTrue(printedOutput.contains("Invalid input: empty input."));
  }

  // deleteTrain
  /**
   * Tests {@link TrainController#deleteTrainByTrainId(Scanner)} with a valid train ID,
   * expects the train list size to be reduced by 1.
   */
  @Test
  void deleteTrainByTrainID_ValidTrainID_DeletesTrain() {
    scanner = simScan("1\n");
    int priorSize = trainRegister.getSortedTrainList().size();
    TrainController trainController = new TrainController(time, trainRegister);

    trainController.deleteTrainByTrainId(scanner);
    int postSize = trainRegister.getSortedTrainList().size();

    assertEquals(priorSize -1, postSize, "Train list size should be 0");
  }

  /**
   * Tests {@link TrainController#deleteTrainByTrainId(Scanner)} with the return command at the first parameter,
   * expects the train list size to remain unchanged.
   */
  @Test
  void deleteTrainByTrainID_ReturnCommand_NoTrainDeleted() {
    scanner = simScan("return\n1\n");
    int priorSize = trainRegister.getSortedTrainList().size();
    TrainController trainController = new TrainController(time, trainRegister);

    trainController.deleteTrainByTrainId(scanner);
    int postSize = trainRegister.getSortedTrainList().size();

    assertEquals(priorSize, postSize, "Train list size should be 1");
  }

  /**
   * Tests {@link TrainController#deleteTrainByTrainId(Scanner)} with two invalid inputs,
   * expects the prompt to loop until a valid input is given.
   */
  @Test
  void deleteTrainByTrainID_InvalidInputLoop_LoopsUntilValid() {
    scanner = simScan("notAnInteger\nnotAnInteger\n1\n");
    TrainController trainController = new TrainController(time, trainRegister);

    trainController.deleteTrainByTrainId(scanner);

    assertEquals(0, trainRegister.getSortedTrainList().size(), "Train list size should be 0");
  }

  /**
   * Tests {@link TrainController#deleteTrainByTrainId(Scanner)} with an invalid train ID,
   * expects the train not found error message to be printed.
   */
  @Test
  void deleteTrainByTrainID_InvalidTrainID_PrintsErrorMessage() {
    scanner = simScan("999\nreturn\n");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.deleteTrainByTrainId(scanner);
    });

    assertTrue(printedOutput.contains("Invalid input: Train ID: 999 not found."));
  }

  /**
   * Tests {@link TrainController#deleteTrainByTrainId(Scanner)} with an invalid input for the train ID parameter,
   * expects integer error message to be printed.
   */
  @Test
  void deleteTrainByTrainID_InvalidInputString_PrintsIntegerMessage() {
    scanner = simScan("notAnInteger\nreturn\n");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.deleteTrainByTrainId(scanner);
    });

    assertTrue(printedOutput.contains("Invalid input: not an integer."));
  }

  // updateTime
  @Test
  void updateTime_ValidInput_UpdatesTime() {
    scanner = simScan("14:30\n");
    TrainController trainController = new TrainController(time, trainRegister);
    trainController.updateTime(scanner);
    LocalTime expectedTime = LocalTime.parse("14:30");

    assertEquals(expectedTime, time.getTime(), "Time should be updated to 14:30");
  }

  /**
   * Tests {@link TrainController#updateTime(Scanner)} with the return command at the first parameter,
   * expects the time to remain unchanged (00:00).
   */
  @Test
  void updateTime_ReturnCommand_TimeNotUpdated() {
    scanner = simScan("return\n14:30\n");
    TrainController trainController = new TrainController(time, trainRegister);
    LocalTime defaultTime = LocalTime.parse("00:00");

    trainController.updateTime(scanner);

    assertEquals(defaultTime, time.getTime(), "Time should be 00:00");
  }

  /**
   * Tests {@link TrainController#updateTime(Scanner)} with an invalid input for the time parameter,
   * expects {@link TimeFormatException#getMessage()} to be printed.
   */
  @Test
  void updateTime_InvalidInputCatchesException_PrintsExceptionMessage() {
    scanner = simScan("25:91\nreturn\n");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.updateTime(scanner);
    });

    assertTrue(printedOutput.contains("Invalid input: invalid time format."));
  }

  /**
   * Tests {@link TrainController#updateTime(Scanner)} with two invalid inputs, expects method to loop until
   * a valid input is provided.
   */
  @Test
  void updateTime_InvalidInputCatchesException_LoopsUntilValid() {
    scanner = simScan("25:91\ninvalid\n14:30\n");
    TrainController trainController = new TrainController(time, trainRegister);
    trainController.updateTime(scanner);

    LocalTime expectedTime = LocalTime.parse("14:30");
    assertEquals(expectedTime, time.getTime(), "Time should be updated to 14:30");
  }

  /**
   * Tests {@link TrainController#updateTime(Scanner)} with an empty input for the time parameter,
   * expects empty input error message to be printed.
   */
  @Test
  void updateTime_InvalidInputEmpty_PrintsEmptyMessage() {
    scanner = simScan("\nreturn\n");

    // Use helper method to capture printed output
    String printedOutput = capOutput(() -> {
      TrainController trainController = new TrainController(time, trainRegister);
      trainController.updateTime(scanner);
    });

    assertTrue(printedOutput.contains("Invalid input: empty input."));
  }
}