package edu.ntnu.stud;

import org.junit.jupiter.api.Test;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import static org.junit.jupiter.api.Assertions.*;

public class TrainDispatchAppTest {

  /**
   * Helper method for simulating scanner input.
   *
   * @param input The input string to be simulated.
   * @see System#setIn(InputStream)
   */
  private void simInput(String input) {
    InputStream in = new ByteArrayInputStream(input.getBytes());
    System.setIn(in);
  }

  /**
   * Helper method for capturing printed output.
   *
   * @param action Performed for capturing output.
   * @return Captured output as a string.
   */
  private static String capOutput(Runnable action) {
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    System.setOut(new PrintStream(outputStream));
    action.run();
    System.setOut(System.out);
    return outputStream.toString().trim();
  }


  @Test
  void Main_EntryAndExit_EntersAndExits() {
    simInput("9\n");

    assertDoesNotThrow(() -> TrainDispatchApp.main(null));
  }

  @Test
  void Main_PrintsMenu() {
    simInput("9\n");
    String printedOutput = capOutput(() -> TrainDispatchApp.main(null));

    assertTrue(printedOutput.contains(
        """
            Main Menu:
            1. Show train overview
            2. Add new train
            3. Allocate track to train
            4. Allocate delay to train
            5. Search for expected departure using train ID
            6. Search for expected departure using destination
            7. Delete train from list
            8. Update time
            9. Exit
            Enter your choice:\s"""));
  }
}