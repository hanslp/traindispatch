package edu.ntnu.stud.view;

import edu.ntnu.stud.exceptions.BackInTimeException;
import edu.ntnu.stud.models.Time;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Functionality test of the methods in {@link Menu} class, including simulated scenarios.
 * <p>
 * {@link Menu#init()}
 * {@link Menu#start()}
 * </p>
 */
public class MenuTest {

  /**
   * Helper method for simulating scanner input.
   *
   * @param input The input string to be simulated.
   * @see System#setIn(InputStream)
   */
  private void simInput(String input) {
    InputStream in = new ByteArrayInputStream(input.getBytes());
    System.setIn(in);
  }

  /**
   * Helper method for capturing printed output.
   *
   * @param action Performed for capturing output.
   * @return Captured output as a string.
   */
  private static String capOutput(Runnable action) {
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    System.setOut(new PrintStream(outputStream));
    action.run();
    System.setOut(System.out);
    return outputStream.toString().trim();
  }

  /**
   * Ensures clean and consistent state after each test.
   */
  @AfterEach
  void tearDown() {
    System.setIn(System.in);
  }

  /**
   * Tests initialization and start of the menu loop, expecting the main menu to be printed.
   * <p>
   *   To allow the test method to end, the user input '9' is simulated after initialization.
   * </p>
   * @see Menu#init()
   * @see Menu#start()
   */
  @Test
  void initStart_Success() {
    simInput("9\n");
    Menu menu = new Menu();
    menu.init();
    String printedOutput = capOutput(menu::start);

    assertTrue(printedOutput.contains(
        """
            Main Menu:
            1. Show train overview
            2. Add new train
            3. Allocate track to train
            4. Allocate delay to train
            5. Search for expected departure using train ID
            6. Search for expected departure using destination
            7. Delete train from list
            8. Update time
            9. Exit
            Enter your choice:\s"""));
  }

  /**
   * Tests that the exit message is printed when exiting the program.
   */
  @Test
  void ExitMenu_PrintsExitMessage() {
    simInput("9\n");
    Menu menu = new Menu();
    String printedOutput = capOutput(menu::start);

    assertTrue(printedOutput.contains("Exiting program."));
  }

  /**
   * Tests that the overview is not printed after having exited the program,
   * expects the overview NOT to be printed.
   */
  @Test
  void ExitMenu_Success() {
    // Overview option is selected after having exited the program
    simInput("9\n1\n");
    Menu menu = new Menu();
    String printedOutput = capOutput(menu::start);

    assertFalse(printedOutput.contains("""
        ————————————————————————————(Time: 00:00)—————————————————————————————
        | Train ID | Destination      | Departure | Expected | Line  | Track |
        ——————————————————————————————————————————————————————————————————————
        ——————————————————————————————————————————————————————————————————————"""));
  }

  @Test
  void InvalidChoice_PrintsInvalidMessage() {
    simInput("999\n9\n");
    Menu menu = new Menu();
    String printedOutPut = capOutput(menu::start);

    assertTrue(printedOutPut.contains("Invalid input: please enter a valid choice."));
  }

  /**
   * Tests the return command from within the loops of options 2—8.
   * <p>
   * This test iterates through choices 2 to 8 by simulating user input for each choice followed by 'return'.<br>
   * It checks whether the program prints the expected messages indicating a return
   * to the main menu and program exit.
   * </p>
   */
  @Test
  void ReturnFromLoop_ReturnsToMain() {
    for (int choice = 2; choice < 9; choice++) {
      simInput(choice + "\nreturn\n9\n");
      Menu menu = new Menu();
      menu.init();

      String printedOutput = capOutput(menu::start);

      assertTrue(printedOutput.contains("Returning to the main menu."));
      assertTrue(printedOutput.contains("Exiting program."));
    }
  }

  /**
   * Tests the scenario where the user attempts to add a new train with a non-unique Train ID, expecting a
   * non-unique Train ID error message to be printed.
   *<p>
   *   The simulated input sequence is as follows:
   *   <ol>
   *     <li> 2 (Add a new train) </li>
   *     <li> 13:37 (Enter a departure time) </li>
   *     <li> L1 (Enter a line) </li>
   *     <li> 1 (Enter a train ID) </li>
   *     <li> return (Re-prompted to enter a train-ID because non-unique) </li>
   *     <li> 9 (Exit program) </li>
   *   </ol>
   */
  @Test
  void AddTrain_NonUniqueTrainID_PrintsErrorMessage() {
    // 2 -> AddTrain, 13:37 -> DepartureTime, L1 -> Line, 1 -> TrainId
    simInput("2\n13:37\nL1\n1\nreturn\n9\n");
    Menu menu = new Menu();
    menu.init();

    String printedOutput = capOutput(menu::start);

    assertTrue(printedOutput.contains("Invalid input: Train ID already in use."));
    assertTrue(printedOutput.contains("Returning to the main menu."));
    assertTrue(printedOutput.contains("Exiting program."));
  }

  /**
   * Tests choice 1, expecting the overview to be printed.
   */
  @Test
  void ChoiceOne_PrintsOverview() {
    simInput("1\n9\n");
    Menu menu = new Menu();
    menu.init();
    String printedOutput = capOutput(menu::start);

    assertTrue(printedOutput.contains(
        """
            ————————————————————————————(Time: 00:00)—————————————————————————————
            | Train ID | Destination      | Departure | Expected | Line  | Track |
            ——————————————————————————————————————————————————————————————————————
            | 2        | Oslo             | 16:00     | 16:05    | L2    | 2     |
            | 1        | Oslo             | 17:00     | On time  | L1    | TBD   |
            | 3        | Trondheim        | 16:59     | 17:01    | L3    | 3     |
            ——————————————————————————————————————————————————————————————————————"""));
  }

  @Test
  void ChoiceTwo_PrintsAddTrain() {
    simInput("2\nreturn\n9\n");
    Menu menu = new Menu();
    menu.init();

    String printedOutput = capOutput(menu::start);

    assertTrue(printedOutput.contains("Add new train (Enter 'return' to go back to the main menu)"));
    assertTrue(printedOutput.contains("Returning to the main menu."));
    assertTrue(printedOutput.contains("Exiting program."));
  }

  @Test
  void ChoiceThree_PrintsAllocateTrack() {
    simInput("3\nreturn\n9\n");
    Menu menu = new Menu();
    menu.init();

    String printedOutput = capOutput(menu::start);

    assertTrue(printedOutput.contains("Allocate track (Enter 'return' to go back to the main menu)"));
    assertTrue(printedOutput.contains("Returning to the main menu."));
    assertTrue(printedOutput.contains("Exiting program."));
  }

  @Test
  void ChoiceFour_PrintsAllocateDelay() {
    simInput("4\nreturn\n9\n");
    Menu menu = new Menu();
    menu.init();

    String printedOutput = capOutput(menu::start);

    assertTrue(printedOutput.contains("Allocate delay (Enter 'return' to go back to the main menu)"));
    assertTrue(printedOutput.contains("Returning to the main menu."));
    assertTrue(printedOutput.contains("Exiting program."));
  }

  /**
   * Tests the scenario where the user attempts to allocate a delay to a non-existent train,
   * expecting a non-existent train error message to be printed.
   * <p>
   *   The simulated input sequence is as follows:
   *   <ol>
   *     <li> 4 (Allocate delay) </li>
   *     <li> 999 (Enter a train ID) </li>
   *     <li> return (Re-prompted to enter a train-ID because non-existent) </li>
   *     <li> 9 (Exit program) </li>
   *    </ol>
   */
  @Test
  void AllocateDelay_NonExistentTrain__PrintsErrorMessage() {
    simInput("4\n999\nreturn\n9\n");
    Menu menu = new Menu();
    menu.init();

    String printedOutput = capOutput(menu::start);

    assertTrue(printedOutput.contains("Invalid input: Train ID: 999 not found."));
    assertTrue(printedOutput.contains("Returning to the main menu."));
    assertTrue(printedOutput.contains("Exiting program."));
  }

  @Test
  void ChoiceFive_PrintsGetDepartureByTrainId() {
    simInput("5\nreturn\n9\n");
    Menu menu = new Menu();
    menu.init();

    String printedOutput = capOutput(menu::start);

    assertTrue(printedOutput.contains("Search for expected departure "
        + "(Enter 'return' to go back to the main menu)"));
    assertTrue(printedOutput.contains("Returning to the main menu."));
    assertTrue(printedOutput.contains("Exiting program."));
  }

  @Test
  void ChoiceSix_PrintsGetDepartureByDestination() {
    simInput("6\nreturn\n9\n");
    Menu menu = new Menu();
    menu.init();

    String printedOutput = capOutput(menu::start);

    assertTrue(printedOutput.contains("Search for train departure (Enter 'return' to go back to the main menu)"));
    assertTrue(printedOutput.contains("Returning to the main menu."));
    assertTrue(printedOutput.contains("Exiting program."));
  }

  @Test
  void ChoiceSeven_PrintsDeleteTrainByTrainId() {
    simInput("7\nreturn\n9\n");
    Menu menu = new Menu();
    menu.init();

    String printedOutput = capOutput(menu::start);

    assertTrue(printedOutput.contains("Delete train from train list (enter 'return' to exit): "));
    assertTrue(printedOutput.contains("Returning to the main menu."));
    assertTrue(printedOutput.contains("Exiting program."));
  }

  @Test
  void ChoiceEight_PrintsUpdateTime() {
    simInput("8\nreturn\n9\n");
    Menu menu = new Menu();
    menu.init();

    String printedOutput = capOutput(menu::start);

    assertTrue(printedOutput.contains("Update time (Enter 'return' to go back to the main menu)"));
    assertTrue(printedOutput.contains("Returning to the main menu."));
    assertTrue(printedOutput.contains("Exiting program."));
  }

  /**
   * Tests a scenario where the time is updated to a time before the current time,
   * expecting the message from {@link BackInTimeException#getMessage()} to be printed.
   * <p>
   *   The simulated input sequence is as follows:
   *   <ol>
   *     <li> 8 (Update time) </li>
   *     <li> 09:00 (Enter a new time) </li>
   *     <li> 8 (Update time) </li>
   *     <li> 08:00 (Enter a new time) </li>
   *     <li> return (Re-prompted to enter a new time because before current time) </li>
   *     <li> 9 (Exit program) </li>
   *     </ol>
   *  </p>
   *
   */
  @Test
  void updateTime_BeforeCurrent_ErrorMessagePrinted() {
    simInput("8\n09:00\n8\n08:00\nreturn\n9\n");
    Menu menu = new Menu();
    menu.init();

    String printedOutput = capOutput(menu::start);

    assertTrue(printedOutput.contains("Invalid input: updated time must be later than current time."));
  }

  /**
   * Tests a scenario where the user adds a new train, allocates a track to the train, and allocates a delay to the train.
   * <p>
   *   The simulated input sequence is as follows:
   *   <ol>
   *     <li> 2 (Add a new train) </li>
   *     <li> 13:37 (Enter a departure time) </li>
   *     <li> L1 (Enter a line) </li>
   *     <li> 4 (Enter a train ID) </li>
   *     <li> Oslo (Enter a destination) </li>
   *     <li> 1 (Enter a track) </li>
   *     <li> 3 (Allocate track) </li>
   *     <li> 4 (Enter a train ID) </li>
   *     <li> 2 (Enter a new track) </li>
   *     <li> 4 (Allocate delay) </li>
   *     <li> 4 (Enter a train ID) </li>
   *     <li> 00:10 (Enter a delay) </li>
   *     <li> 9 (Exit program) </li>
   *   </ol>
   */
  @Test
  void MultipleOperations_AddTrain_AllocateTrack_AllocateDelay() {
    // 2 -> AddTrain, 13:37 -> DepartureTime, L1 -> Line, 4 -> TrainId, Oslo -> Destination, 1 -> Track,
    // 3 -> AllocateTrack, 4 -> TrainID, 2 -> New Track,
    // 4 -> AllocateDelay, 4 -> TrainID, 00:10 -> Delay,
    // 9 -> Exit
    simInput("2\n13:37\nL1\n4\nOslo\n1\n3\n4\n2\n4\n4\n00:10\n9\n");
    Menu menu = new Menu();
    menu.init();

    String printedOutput = capOutput(menu::start);

    // Verify that the train is added, track is allocated, and delay is allocated
    assertTrue(printedOutput.contains("Add new train (Enter 'return' to go back to the main menu)"));
    assertTrue(printedOutput.contains("New train added with train ID: 4"));

    assertTrue(printedOutput.contains("Allocate track (Enter 'return' to go back to the main menu)"));
    assertTrue(printedOutput.contains("Train ID: 4, updated track to: 2"));

    assertTrue(printedOutput.contains("Allocate delay (Enter 'return' to go back to the main menu)"));
    assertTrue(printedOutput.contains("Train ID: 4, new delay: 00:10, new expected departure: 13:47"));

    assertTrue(printedOutput.contains("Exiting program."));

    assertFalse(printedOutput.contains("Returning to the main menu."));
    assertFalse(printedOutput.contains("Invalid input: please enter a valid choice."));
    assertFalse(printedOutput.contains("Invalid input: Train ID already in use."));
  }
}