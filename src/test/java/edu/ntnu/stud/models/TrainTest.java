package edu.ntnu.stud.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import edu.ntnu.stud.exceptions.NextDayException;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

/**
 * Functionality test of the methods in the {@link Train} class:
 * <ul>
 *   <li>{@link Train#setDelay(String)}</li>
 *   <li>{@link Train#getExpectedDeparture()}</li>
 *   <li>{@link Train#setTrack(int)}</li>
 *   <li>{@link Train#getDepartureTime()}</li>
 *   <li>{@link Train#getLine()}</li>
 *   <li>{@link Train#getTrainId()}</li>
 *   <li>{@link Train#getDestination()}</li>
 *   <li>{@link Train#getDelay()}</li>
 *   <li>{@link Train#getTrack()}</li>
 * </ul>
 *
 * @see NextDayException
 */
public class TrainTest {
  private Train train;

  /**
   * Ensures clean and consistent test environment.
   * Sets up a new {@link Train} object before each test.
   */
  @BeforeEach
  void setUp() {
    train = new Train("12:00", "L1", 1, "Oslo", 2, "00:10");
  }

  /**
   * Tests {@link Train#setDelay(String)} with a valid delay, expecting the delay to be set and
   * no exception to be thrown.
   *
   */
  @Test
  void setDelay_ValidDelay_Success() {
    assertDoesNotThrow(() -> train.setDelay("01:30"));
    assertEquals("01:30", train.getDelay());
  }

  /**
   * Tests {@link Train#setDelay(String)} with an invalid delay (12:00+20:00 exceeds 24 hrs)
   * expecting a {@link NextDayException} to be thrown.
   */
  @Test
  void setDelay_InvalidDelay_ExceptionThrown() {
    assertThrows(NextDayException.class, () -> train.setDelay("20:00"));
  }

  /**
   * Tests {@link Train#getExpectedDeparture()} with a valid delay,
   * expecting the correct expected departure time and no exception to be thrown.
   */
  @Test
  void getExpectedDeparture_ValidDelay_CorrectExpectedDeparture() {
    assertDoesNotThrow(() -> train.setDelay("00:20"));
    assertEquals("12:20", train.getExpectedDeparture());
  }

  @Test
  void setTrack_ValidTrack_Success() {
    train.setTrack(5);
    assertEquals(5, train.getTrack());
  }

  //basic getters
  @Test
  void getDepartureTime_ReturnsDepartureTime() {
    assertEquals("12:00", train.getDepartureTime());
  }

  @Test
  void getLine_ReturnsLine() {
    assertEquals("L1", train.getLine());
  }

  @Test
  void getTrainID_ReturnsTrainID() {
    assertEquals(1, train.getTrainId());
  }

  @Test
  void getDestination_ReturnsDestination() {
    assertEquals("Oslo", train.getDestination());
  }

  @Test
  void getDelay_ReturnsDelay() {
    assertEquals("00:10", train.getDelay());
  }

  @Test
  void getTrack_ReturnsTrack() {
    assertEquals(2, train.getTrack());
  }
}