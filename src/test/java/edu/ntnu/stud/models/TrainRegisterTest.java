package edu.ntnu.stud.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * Functionality test of the {@link TrainRegister} class.
 * <ul>
 *   <li>{@link TrainRegister#addTrainToList(String, String, int, String, int, String)}</li>
 *   <li>{@link TrainRegister#getSortedTrainList()}</li>
 *   <li>{@link TrainRegister#searchTrainId(int)}</li>
 *   <li>{@link TrainRegister#searchDestination(String)}</li>
 *   <li>{@link TrainRegister#deleteTrainFromList(int)}</li>
 *   <li>{@link TrainRegister#isTrainIdUnique(int)}</li>
 * </ul>
 */
public class TrainRegisterTest {
  private static TrainRegister trainRegister;

    /**
     * Ensures clean and consistent test environment.
     * Creates new {@link TrainRegister} object before each test.
     * Populates the register with three dummy trains.
     */
  @BeforeEach
  void setUp() {
    trainRegister = new TrainRegister();
    trainRegister.addTrainToList("17:00", "L1", 1, "Oslo", -1, "00:00");
    trainRegister.addTrainToList("16:00", "L2", 2, "Oslo", 2, "00:05");
    trainRegister.addTrainToList("16:59", "L3", 3, "Trondheim", 3, "00:02");
  }

  /**
   * Tests {@link TrainRegister#getSortedTrainList()} expecting the returned
   * list to be sorted by expected departure time.
   */
  @Test
  void getSortedTrainList_ReturnSortedList_Success() {
    // Index shows order of trains in list
    assertEquals("16:05", trainRegister.getSortedTrainList().get(0).getExpectedDeparture());
    assertEquals("17:00", trainRegister.getSortedTrainList().get(1).getExpectedDeparture());
    assertEquals("17:01", trainRegister.getSortedTrainList().get(2).getExpectedDeparture());
  }

  /**
   * Tests {@link TrainRegister#addTrainToList(String, String, int, String, int, String)}
   * expecting the size of the train list to increase by one.
   */
  @Test
  void addTrainToList_Success() {
    int postSize = trainRegister.getSortedTrainList().size();
    trainRegister.addTrainToList("12:00", "L4", 4, "Bergen", 4, "00:00");
    int priorSize = trainRegister.getSortedTrainList().size();

    assertEquals(postSize + 1, priorSize);
  }
  /**
   * Tests {@link TrainRegister#searchTrainId(int)} with an existing train ID,
   * expecting the train with the provided train ID to be returned.
   */
  @Test
  void searchTrainID_ExistingTrainID_ReturnsTrain() {
    Train foundTrain = trainRegister.searchTrainId(2);

    assertEquals(2, foundTrain.getTrainId());
  }

  /**
   * Tests {@link TrainRegister#searchTrainId(int)} with a non-existing train ID,
   * expecting null to be returned.
   */
  @Test
  void searchTrainID_NonExistingTrainID_ReturnsNull() {
    Train foundTrain = trainRegister.searchTrainId(10);
    assertNull(foundTrain);
  }

  /**
   * Tests {@link TrainRegister#searchDestination(String)} with an existing destination,
   * expecting a list of trains with the provided destination to be returned.
   */
  @Test
  void searchDestination_ExistingDestination_ReturnsDepartureSortedList() {
    String destination = "Oslo";
    assertEquals("16:05", trainRegister.searchDestination(destination).get(0).getExpectedDeparture());
    assertEquals("17:00", trainRegister.searchDestination(destination).get(1).getExpectedDeparture());
  }

  /**
   * Tests {@link TrainRegister#searchDestination(String)} with a non-existing destination,
   * expecting an empty list to be returned.
   */
  @Test
  void searchDestination_NonExistingDestination_ReturnsEmptyList() {
    String destination = "Bergen";
    assertTrue(trainRegister.searchDestination(destination).isEmpty());
  }

  /**
   * Tests {@link TrainRegister#deleteTrainFromList(int)} with an existing train ID,
   * expecting the size of the train list to be reduced by one.
   */
  @Test
  void deleteTrainFromList_ExistingTrainID_Success() {
    int priorSize = trainRegister.getSortedTrainList().size();
    assertTrue(trainRegister.deleteTrainFromList(2));
    int postSize = trainRegister.getSortedTrainList().size();

    assertEquals(priorSize - 1, postSize);
  }

  /**
   * Tests {@link TrainRegister#deleteTrainFromList(int)} with a non-existing train ID,
   * expecting the size of the train list to remain unchanged.
   */
  @Test
  void deleteTrainFromList_NonExistingTrainID_Failure() {
    int priorSize = trainRegister.getSortedTrainList().size();
    assertFalse(trainRegister.deleteTrainFromList(10));
    int postSize = trainRegister.getSortedTrainList().size();

    assertEquals(priorSize, postSize);
  }

  /**
   * Tests {@link TrainRegister#isTrainIdUnique(int)} with a unique train ID,
   * expecting true to be returned.
   */
  @Test
  void isTrainIDUnique_UniqueTrainID_ReturnsTrue() {
    assertTrue(trainRegister.isTrainIdUnique(4));
  }

  /**
   * Tests {@link TrainRegister#isTrainIdUnique(int)} with a non-unique train ID,
   * expecting false to be returned.
   */
  @Test
  void isTrainIDUnique_NotUniqueTrainID_ReturnsFalse() {
    assertFalse(trainRegister.isTrainIdUnique(2));
  }
}