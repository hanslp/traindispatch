package edu.ntnu.stud.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import edu.ntnu.stud.exceptions.TimeFormatException;
import edu.ntnu.stud.exceptions.BackInTimeException;
import java.time.format.DateTimeFormatter;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

/**
 * Functionality test of the methods in the {@link Time} class.
 * <ul>
 *   <li>{@link Time#validateTime(String)}</li>
 *   <li>{@link Time#setSystemTime(String)}</li>
 *   <li>{@link Time#getTime()}</li>
 * </ul>
 * @see TimeFormatException
 * @see BackInTimeException
 */
public class TimeTest {
  private Time time;
  private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");

  /**
   * Ensures clean and consistent test environment.
   * Creates new {@link Time} object before each test.
   */
  @BeforeEach
  void setUp() {
    time = new Time();
  }

  /**
   * Tests {@link Time#getTime()} without having set any time prior, expecting the default time to be returned.
   */
  @Test
  void getTime_DefaultTime_ReturnsDefaultTime() {
    assertEquals("00:00", time.getTime().format(TIME_FORMATTER));
  }

  /**
   * Tests {@link Time#validateTime(String)} with an invalid time, expecting a {@link TimeFormatException} to be thrown.
   */
  @Test
  void validateTime_InvalidTimeFormat_ExceptionThrown() {
    assertThrows(TimeFormatException.class, () -> Time.validateTime("Charizard"));
  }

  /**
   * Tests {@link Time#validateTime(String)} with an invalid time duration (25:00),
   * expecting a {@link TimeFormatException} to be thrown.
   */
  @Test
  void validateTime_InvalidTimeDuration_ExceptionThrown() {
    assertThrows(TimeFormatException.class, () -> Time.validateTime("25:00"));
  }

  /**
   * Tests {@link Time#validateTime(String)} with a special case time (24:00),
   * expecting a {@link TimeFormatException} to be thrown.
   */
  @Test
  void validateTime_SpecialCaseTime_ExceptionThrown() {
    assertThrows(TimeFormatException.class, () -> Time.validateTime("24:00"));
  }

  /**
   * Tests {@link Time#setSystemTime(String)} with a valid time, expecting the time to be set and no exception to be thrown.
   */
  @Test
  void setSystemTime_ValidTime_Success() throws BackInTimeException {
    time.setSystemTime("12:30");
    assertEquals("12:30", time.getTime().format(TIME_FORMATTER));
    assertDoesNotThrow(() -> Time.validateTime("12:30"));
  }

  /**
   * Tests {@link Time#setSystemTime(String)} in the scenario where the time is updated
   * to a time before the current time, expecting a {@link BackInTimeException} to be thrown.
   */
  @Test
  void setSystemTime_BeforeCurrent_ExceptionThrown() throws BackInTimeException {
    time.setSystemTime("12:30");
    assertThrows(BackInTimeException.class, () -> time.setSystemTime("09:00"));
  }

  /**
   * Tests {@link Time#setSystemTime(String)} in the scenario where the time is updated
   * to a time equal to the current time, expecting a {@link BackInTimeException} to be thrown.
   */
  @Test
  void setSystemTime_EqualToCurrent_ExceptionThrown() throws BackInTimeException {
    time.setSystemTime("12:30");
    assertThrows(BackInTimeException.class, () -> time.setSystemTime("12:30"));
  }
}