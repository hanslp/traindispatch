# Portfolio project IDATA1003 - 2023

STUDENT NAME: Hans Lawrence Pettersen <br>
STUDENT ID: 539102

## Project description
The Train Dispatch app is a simplified version of a train dispatch system. 
It provides a text-based user interface with services for managing train dispatch information.

## Project structure
The project is, roughly speaking, structured according to the Model-View-Controller (MVC) architecture.

![uml_v5 pic.png](uml%2Fuml_v5%20pic.png)

_src/main/java/edu.ntnu.stud_ contains the following:
* **TrainDispatchApp**: contains the <code>main</code> method.
* models package: **Time, Train, TrainRegister**
* controller package: **TrainController, InputValidator**
* view package: **Menu**
* exceptions package: **BackInTimeException, NextDayException, TimeFormatException**

_src/test/java/edu.ntnu.stud_ contains test methods for all of the above.

## Link to repository
https://gitlab.stud.idi.ntnu.no/hanslp/traindispatch/-/tree/main

## How to run the project
1. Run the <code>main</code> method found in the **TrainDispatchApp** class. <br>
   path: _src/main/java/edu/ntnu/stud/TrainDispatchApp.java_<br>
   GitLab link to **TrainDispatchApp**: https://gitlab.stud.idi.ntnu.no/hanslp/traindispatch/blob/8b2cbf0a2d26ca74f480c21fc735fbc716d0678f/src/main/java/edu/ntnu/stud/TrainDispatchApp.java
2. The program will present a menu of options. <br>
   ```
   Main Menu:
   1. Show train overview
   2. Add new train
   3. Allocate track to train
   4. Allocate delay to train
   5. Search for expected departure using train ID
   6. Search for expected departure using destination
   7. Delete train from list
   8. Update time
   9. Exit
   Enter your choice:
      ```
3. Select an option by entering the corresponding number.
4. The overview (option 1) compares the expected departure time with a reference time. <br>
    Only trains that depart after or _at_ the reference time are shown. <br>
    ```
   ————————————————————————————(Time: 00:00)—————————————————————————————
    | Train ID | Destination      | Departure | Expected | Line  | Track |
    ——————————————————————————————————————————————————————————————————————
    | 2        | Oslo             | 16:00     | 16:05    | L2    | 2     |
    | 1        | Oslo             | 17:00     | On time  | L1    | TBD   |
    | 3        | Trondheim        | 16:59     | 17:01    | L3    | 3     |
    ——————————————————————————————————————————————————————————————————————
   ```
5. The reference time is by default set to 00:00. Update it by selecting option 8.
6. Options 2—8 have sub-menus. For example: <br>
   ```
   2. Add new train
   ```
    Gives a sub-menu for each parameter required to create a new train object: <br>
    ```   
   Add new train (Enter 'return' to go back to the main menu)
    Enter departure time (hh:mm):
   ```
   These sub-menus will loop until a valid input or 'return' is entered.

## How to run the tests
Run all the tests in the package edu.ntnu.stud <br>
path: _src/test/java/edu/ntnu/stud_<br>
GitLab link to tests: https://gitlab.stud.idi.ntnu.no/hanslp/traindispatch/blob/f4530447d0ae8530f31974638868aeaeec43c796/src/test/java/edu/ntnu/stud